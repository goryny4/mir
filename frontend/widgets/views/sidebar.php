<?php $lang = Yii::app()->getLanguage();
foreach ($data as $continent):?>
	<div class="indent">
		<h4><?=$continent["name_$lang"]?></h4>
		<div class="wrapper">
			<ul>
				<?php if (isset($continent['countries'])) foreach ($continent['countries'] as $country):?>
					<li><a href="/<?=$entity?>/country/<?=$country['id']?>"><?=CHtml::tag($country['selected']?'b':'span',array(),$country["name_$lang"])?></a> (<?=$country["{$entity}Count"]?>)</li>
				<?php endforeach;?>
			</ul>
		</div>
	</div>
<?php endforeach;?>
