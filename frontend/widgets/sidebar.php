<?php
/**
 * Created by PhpStorm.
 * User: GoRyNy4
 * Date: 10.11.13
 * Time: 11:48
 */


class sidebar extends CWidget
{

	public $entity;
	public $selectedCountryId = 0;
	public $height;
	public $quality;
	public $class;
	public $lazy;

	public function init()
	{
		$continents = Continents::model()->findAll();
		$result = array();
		foreach ($continents as $continent) {
			$continentArray = array();
			foreach ($continent->countries as $country) {
				if ($count = $country->{$this->entity.'Count'}) {
					$countryArray = array();
					$countryArray['id'] = $country->id;
                    $countryArray['name_ru'] = $country->name_ru;
                    $countryArray['name_en'] = $country->name_en;
					$countryArray[$this->entity.'Count'] = $count;
					$countryArray['selected'] = (($country->id == $this->selectedCountryId)?1:0);

					$continentArray['countries'][] = $countryArray;
				}
			}
			if (isset($continentArray['countries'])) {
				$continentArray['id'] = $continent->id;
                $continentArray['name_ru'] = $continent->name_ru;
                $continentArray['name_en'] = $continent->name_en;

                $result[] = $continentArray;
			}
		}

		$this->render('sidebar',array(
			'data' => $result,
			'entity' => $this->entity,
		));
	}
}
