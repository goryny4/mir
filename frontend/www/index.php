<?php

/**
 * Bootstrap index file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */


require('./../../common/lib/vendor/autoload.php');

defined('APPLICATION_ENV')
||
define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));



/**
 * Включим дебаг если мы разработчики
 */

$variables = array(
	'STATIC_PATH'=>array(
		'dev'=>"/home/vagrant/Code/mir.static/",
		'production'=>"/var/www/mir.static/"
	),
	'STATIC_URL'=>array(
		'dev'=>"http://static.mir.app/",
		'production'=>"http://static.mir-prikliuchenii.com/"
	),
	'YII_DEBUG'=>array(
		'dev'=>1,
		'production'=>0
	),
	'YII_TRACE_LEVEL'=>array(
		'dev'=>1,
		'production'=>0
	),
);

foreach ($variables as $key=>$values) {
	defined($key) or define($key,$values[APPLICATION_ENV]);
}
/*
define('STATIC_PATH',$variables['STATIC_PATH'][APPLICATION_ENV]);
define('STATIC_PATH',$variables['STATIC_PATH'][APPLICATION_ENV]);
defined('YII_DEBUG') or define('YII_DEBUG',$variables['YII_DEBUG'][APPLICATION_ENV]);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',$variables['YII_TRACE_LEVEL'][APPLICATION_ENV]);
*/
// --- configs --

$configs = array(
    __DIR__ .'/../../common/config/main.php',
    __DIR__ .'/../../common/config/env.php',
);
if (APPLICATION_ENV) $configs[]= __DIR__ .'/../../common/config/env/'.APPLICATION_ENV.'.php';

// ----------
Yiinitializr\Helpers\Initializer::create('./../', 'frontend',$configs)->run();
