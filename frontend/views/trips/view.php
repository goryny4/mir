<?php $this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
);
?>

<div class="content">
	<div class="main">
		<div class="wrapper-box module">
			<div class="clear">
				<div class="boxIndent">
					<div class="box-top">
						<div class="box-top-bg">
							<div class="wrapper">
								<?=CHtml::image($this->assets."/images/banner.png");?>
								<div style="width: 390px">
									<span style="padding-top: 0px"><strong><?=$model->title?></strong></span>
									<span style="background: none"><a style="color:white" href="/trips"><?=Yii::t('main','Фоторепортажи')?></a></span>
								</div>
                                <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
							</div>
						</div>
					</div> </div>
			</div>
		</div>
		<div class="wrapper">
			<ul class="list3">
				<li class="first hr">
					<div style="float:left;">
						<?php echo $this->widget('common.widgets.fancyPic',
							array(
								'path'=> $model->image,
								'width'=> 300,
							),
						true); ?>
					</div>
					<div style="float:left; height: 178px; width:400px; padding-top: 20px">
                        <h4 style="padding-top:0px; padding-bottom:20px;"><?=GxHtml::encode($model->title)?></h4>
                        <strong><?=Translate::dates($model->dates)?></strong>
						<p><?=Yii::t('main','Количество дней')?>: <?=$model->movie->days_spent?></p>
						<ul>
							<li><?=$model->description?></li>
						</ul>
					</div>
					<div class="buttons" style="float:right;">
						<div class="wrapper" style="width: 100%; padding-right: 30px; float: right">
							<!--a target="_blank" href="/movies/<?php //= $model->movie_id?>">Фрагменты из фильма</a-->
							<?php if ($model->forum_url): ?>
								<a target="_blank" href="<?=$model->forum_url?>">Обсудить на Форуме</a>
							<?php endif; ?>
							<?php if ($model->plan_id): ?>
							<a style="font-size: 17px" target="_blank" href="/plans/<?=$model->plan_id?>"><?=Yii::t('main','Программа путешествия')?></a>
							<?php endif; ?>
						</div>
					</div>

				</li>

                <?php if ($mapPath = $model->youtube_map) { ?>
                    <li>
						<h3><?=Yii::t('main','Карта путешествия')?></h3>
						<?=Youtube::getPlayer($model->youtube_map,480,array('start'=>0));?>
					<?php /*	?>
					<div class="trip"><p style="padding-bottom: 10px">Карта маршрута:</p>
					<?php $this->widget('ext.Yiippod.Yiippod', array(
							'video'=>$mapPath,
							'id' => 'yiippodplayer',
							'width'=>880,
							'height'=>580,
							'bgcolor'=>'#000'
						));
					?></div>
                    <?php */ ?>
					</li>
                <?php } ?>
				<li style="padding-top: 0px">
				 <div class="trip">
					 <?php
					 // SCENARIO
					 $this->widget('common.widgets.magicReplace',array(
						 'scenario' => $model->scenario,
						 'photos' => $model->photos
	                 ));?>
				</div>
				</li>
				<li>
					<?php
					// COMMENTS
					$this->widget('ext.disqus.YiiDisqusWidget',array('shortname'=>'mir-prikliuchenii')); ?>
				</li>
			</ul>
		</div>
	</div>
</div>