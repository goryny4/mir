<li>
	<?=GxHtml::link(Yii::app()->easyImage->thumbOf(STATIC_PATH.$data->image,
		array(
			'resize' => array('width' => 250),
			//	'rotate' => array('degrees' => 90),
			//	'sharpen' => 50,
			//	'background' => '#ffffff',
			'type' => 'jpg',
		),array('class' => 'img-indent')), array('view', 'id' => $data->id));?>
	<?=GxHtml::link('<h4 style="padding-top:0px;">'.Yii::t('main','Фоторепортаж').' "'.GxHtml::encode($data->title).'"</h4>', array('view', 'id' => $data->id))?>
	<b><?=Translate::dates($data->dates)?></b>
	<p style="padding-bottom: 10px"><?=Yii::t('main','Количество дней')?>: <?=$data->movie->days_spent?></p>
    <?=GxHtml::encode($data->description); ?>

    <!--ul class="list">
        <li style="line-height: 20px">
            <!--a href="#"-->
                <!--/a-->
        <!--/li-->
        <!--li><a href="#">Tuscany Suites &amp; Casino $20</a></li>
        <li><a href="#">South Point Hotel, Casino, and Spa $42</a></li>
        <li><a href="#">The Palms Casino Resort $ 200</a></li>
        <li><a href="#">Hotel Bijou $119</a></li-->
    <!--/ul-->

</li>