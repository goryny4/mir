<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
//	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
//	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
//	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
//	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php /* $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'movie',
			'type' => 'raw',
			'value' => $model->movie !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->movie)), array('movies/view', 'id' => GxActiveRecord::extractPkValue($model->movie, true))) : null,
			),
'youtube_url',
	),
)); */?>

<iframe width="720" height="480" src="//www.youtube.com/embed/<?=Youtube::getCode($model->youtube_url);?>?autoplay=1&rel=0&border=0&showinfo=0&disablekb=1" frameborder="0" allowfullscreen" frameborder="0" allowfullscreen></iframe>


