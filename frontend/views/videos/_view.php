<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('movie_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->movie)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('youtube_url')); ?>:
	<?php echo GxHtml::encode($data->youtube_url); ?>
	<br />

</div>