<div style="height:1000px;">
<?php

Yii::app()->clientScript->registerCoreScript('jquery');
$options=Array(
	// Default sidebar state
	'sideBarOpen'=>false,

	// ltr = left to right, rtl=right to left
	'direction'=>'ltr',

	// Button visible state
	'buttons'=>array(
		'sidebarToggle'=>false,
		'viewFind'=>false,
		'pageUp'=>false,
		'pageDown'=>false,
		'zoomIn'=>false,
		'zoomOut'=>false,
		'scaleSelect'=>false,
		'presentationMode'=>true,
		'print'=>false,
		'openFile'=>false,
		'download'=>false,
		'viewBookmark'=>false,
	)
);
$this->widget('ext.pdfJs.QPdfJs',array(
	'url'=>'files/1.pdf',
	'options'=>$options,
	))
?>
</div>