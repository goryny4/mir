<?php $this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
); ?>
<div class="content">
    <div class="main">
	    <div class="boxIndent">
		    <div class="box-top">
			    <div class="box-top-bg">
				    <div class="wrapper">
					    <?=CHtml::image($this->assets."/images/banner.png");?>
					    <div style="width: 390px">
						    <span style="padding-top: 0px"><strong><?=$model->title?></strong></span>
						    <span style="background: none"><a href="/plans" style="color: white"><?=Yii::t('main','В планах')?></a></span>
					    </div>
                        <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
				    </div>
			    </div>
		    </div> </div>
        <div class="wrapper">
            <ul class="list3">
                <li class="first myFont" style="font-size: 18px; line-height: 22px">
                    <strong><?=$model->date?></strong>

	                <?php $this->widget('common.widgets.fancyPic',array(
		                'path' => $model->image,
		                'width' => 300,
	                )); ?>
	                <div class="plan">
		                <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		                <?php // echo 'PDF: ';
		            /*    $this->widget('ext.pdfJs.QPdfJs',array(
			                'url'=>'/files/1.pdf',
			                //    'options'=>$options,
		                )) */?>
		                <?=$model->content?>
	                </div>

                </li>
				<li>
					<?php
					// COMMENTS
					$this->widget('ext.disqus.YiiDisqusWidget',array('shortname'=>'mir-prikliuchenii')); ?>

				</li>
                <!--div class="alignright"><a class="readon" href="#">Read More</a></div-->
            </ul>

        </div>
    </div>
</div>


