<?php

$this->breadcrumbs = array(
	Pages::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
);
?>
<div class="content">
	<div class="main">
		<div class="wrapper">
			<div class="boxIndent">
				<div class="box-top">
					<div class="box-top-bg">
						<div class="wrapper">
							<?=CHtml::image($this->assets."/images/banner.png");?>
							<div>
								<span style="padding-top: 0px"><strong><?=Yii::t('main','Карта')?></strong></span>
								<span style="background: none;"><?=Yii::t('main','География путешествий Валерия Ефимова')?></span>
							</div>
                            <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
						</div>
					</div>
				</div>
			</div>
			<ul class="list3">
				<span style="width:880px; color: #000000; font-size: 18px; line-height: 24px; font-family: Calibri">
    <li>
    <?php // -------------- КАРТА --------------------
    Yii::import('common.extensions.egmap.*');

    $centerDolgota = 20.01;
    $centerShirota = 33.86;

    //$iconPath = $this->assets."/icon/palace-2.png";
    $iconPath = $this->assets."/icon/world-marker.png";

    $icon = new EGMapMarkerImage($iconPath);
    $icon->setSize(24, 23);
    $icon->setAnchor(16, 16.5);
    $icon->setOrigin(0, 0);

    $gMap = new EGMap();
    $gMap->zoom = 2;
    $mapTypeControlOptions = array(
        'position'=> EGMapControlPosition::LEFT_BOTTOM,
        'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
    );
    $gMap->mapTypeControlOptions = $mapTypeControlOptions;
    $gMap->setCenter($centerDolgota,$centerShirota);
    $gMap->addEvent(new EGMapEvent('click','closeWindows()'));


    //       $icon = new EGMapMarkerImage("http://google-maps-icons.googlecode.com/files/gazstation.png");

    $places = Places::model()->findAll();
    foreach ($places as $place) {
        if (!isset($place->coordinates)) continue;
        $title = $place->name;
        $coordinates = explode(',',$place->coordinates);
        $marker = new EGMapMarker($coordinates[0], $coordinates[1],array(
            'title' => $title,
            'icon'=>$icon,
            'raiseOnDrag' => true,
            //		'class' => 'markerWindow'
        ));
        // Create GMapInfoWindows
        $info_window_a = new EGMapInfoWindow(
            (($img = $place->getImage())?$this->widget('common.widgets.fancyPic',
                array(
                    'path'=> $img,
                    'width'=> 400,
                ),
                true):'').
            '<span style="font-weight:bold; font-size:15px; padding-bottom: 4px">'.$place->name.'</span><br/>'.
            $place->description
        /*'<div>Home Sweet Home - Moldova!
        <p><a target="_blank" href="'.Yii::app()->request->getBaseUrl(true).'">Подробнее о стране</a></p>
        </div>'*/
        );
        $marker->addHtmlInfoWindow($info_window_a);
        $gMap->addMarker($marker);
    }
    //
    // ext is your protected.extensions folder
    // gmaps means the subfolder name under your protected.extensions folder
    //




    //      // Create marker with label
    //    $marker = new EGMapMarkerWithLabel($dolgota, $shirota, array('title' => 'Home Sweet Home - Moldova!'));

    // enabling marker clusterer just for fun
    // to view it zoom-out the map
    $gMap->enableMarkerClusterer(new EGMapMarkerClusterer(array('minimumClusterSize'=>5)));
    $gMap->setWidth(880);
    $gMap->setHeight(600);

    $gMap->renderMap();
    ?>
</li>

				</span>
			</ul>
		</div>
	</div>
</div>

<script type="text/javascript">
	function closeWindows() {
		jQuery(".gm-style-iw").parent().hide();
	}
</script>
