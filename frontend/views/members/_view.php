<li style="width: 860px; padding-top: 20px; padding-bottom: 20px">
	<div style="float:left">
		<?=Yii::app()->easyImage->thumbOf(STATIC_PATH.$data->image,
			array(
				'resize' => array('width' => 250),
				//	'rotate' => array('degrees' => 90),
				//	'sharpen' => 50,
				//	'background' => '#ffffff',
				'type' => 'jpg',
			),array('class' => 'img-indent membersImg'))
		?>

	</div>
	<div style="float:left;">
		<p style="width: auto;" class="myFont">
			<?=GxHtml::encode($data->name)?> <?=($data->page_id?Chtml::link('(Подробнее)','/members/'.$data->page_id):'');?>
		</p>
		<p><strong><?=GxHtml::encode($data->country)?></strong></p>
		<p><?=GxHtml::encode($data->description)?></p>

	</div>
</li>
