<?php $this->breadcrumbs = array(
	Plans::label(2),
	Yii::t('app', 'Index'),
);
$this->menu = [];
?>

<div class="content">
    <div class="main">
        <div class="wrapper-box module">
            <div class="clear">
	            <div class="boxIndent">
		            <div class="box-top">
			            <div class="box-top-bg">
				            <div class="wrapper">
					            <?=CHtml::image($this->assets."/images/banner.png");?>
					            <div>
						            <span style="padding-top: 0px"><strong><?=Yii::t('main','Члены клуба')?></strong></span>
						            <!--span style="background: none">Выберите приключение</span-->
					            </div>
					            <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
                            </div>
			            </div>
		            </div> </div>
            </div>
        </div>
        <div class="wrapper">
            <ul class="list1">
                    <?php $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'template'=>'{items}',
                        'itemView'=>'_view',
                    )); ?>


            </ul>


        </div>
    </div>
</div>

