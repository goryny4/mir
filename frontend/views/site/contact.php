<div class="content">
	<div class="main">
		<div class="wrapper-box module">
			<div class="clear">
				<div class="boxIndent">
					<div class="box-top">
						<div class="box-top-bg">
							<div class="wrapper">
								<?=CHtml::image($this->assets."/images/banner.png");?>
								<div>
									<span style="padding-top: 0px"><strong><?=Yii::t('main','Написать нам')?></strong></span>
									<!--span style="background: none">Выберите приключение</span-->
								</div>
                                <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
							</div>
						</div>
					</div> </div>
			</div>
		</div>
		<div class="wrapper">
			<ul class="list3">
				<li class="first myFont" style="font-size: 18px; line-height: 22px">
					<div class="trip">


				<?php
				/* @var $this SiteController */
				/* @var $model ContactForm */
				/* @var $form CActiveForm */

				$this->pageTitle = Yii::app()->name . ' - '. Yii::t('main','Написать нам');
				$this->breadcrumbs = array(
                    Yii::t('main','Написать нам'),
				);
				?>


				<?php if(Yii::app()->user->hasFlash('contact')): ?>

					<div class="flash-success">
						<?php echo Yii::app()->user->getFlash('contact'); ?>
					</div>

				<?php else: ?>

					<p style="width: 100%; padding-bottom: 20px"><?=Yii::t('main',
						'Если у Вас есть какие-либо вопросы или предложения по поводу дальнейших путешествий, пожалуйста, напишите нам.<br/>
						Вы также можете связаться с нами через нашу <a href="http://fb.com/mir.prikliuchenii" target="_blank">группу на Facebook</a>')?>
					</p>

					<div class="form" style="width: 100%">

						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'contact-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
							),
						)); ?>

						<!--p class="note" style="width: 100%">Fields with <span class="required">*</span> are required.</p-->

						<div class="row" style="width: 100%; padding: 10px">
							<?php echo $form->errorSummary($model); ?>
						</div>

						<div class="row" style="width: 100%; padding: 10px">
							<?php echo $form->labelEx($model,'name'); ?><br/>
							<?php echo $form->textField($model,'name'); ?>
							<?php echo $form->error($model,'name'); ?>
						</div>

						<div class="row" style="width: 100%; padding: 10px">
							<?php echo $form->labelEx($model,'email'); ?><br/>
							<?php echo $form->textField($model,'email'); ?>
							<?php echo $form->error($model,'email'); ?>
						</div>

						<div class="row" style="width: 100%; padding: 10px">
							<?php echo $form->labelEx($model,'subject'); ?><br/>
							<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
							<?php echo $form->error($model,'subject'); ?>
						</div>

						<div class="row" style="width: 100%; padding: 10px">
							<?php echo $form->labelEx($model,'body'); ?><br/>
							<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
							<?php echo $form->error($model,'body'); ?>
						</div>

						<?php if(CCaptcha::checkRequirements()): ?>
							<div class="row" style="width: 100%; padding: 10px">
								<?php echo $form->labelEx($model,'verifyCode'); ?><br/>
								<div style="width: 150px">
									<?php $this->widget('CCaptcha'); ?>
									<span style="width: 100%">
									<?php echo $form->textField($model,'verifyCode'); ?>
										</span>
								</div><br/>
								<div class="hint" style="width: 100%"><?=Yii::t('main','Пожалуйста, введите текст на картинке.')?>
                                    <br /><?=Yii::t('main','Строчные и прописные буквы равнозначны.')?></div>
								<?php // echo $form->error($model,'verifyCode'); ?>
							</div>
						<?php endif; ?>
						<div class="buttons" style="width: 100%; padding: 10px">
							<div class="wrapper" style="width: 50%; text-align: center">
								<a href="#" id="contact-submit"><?=Yii::t('main','Отправить сообщение')?></a>
							</div>
						</div>


						<?php $this->endWidget(); ?>
						<script type="text/javascript">$j('#contact-submit').click(function() {	$j(this).parents('form:first').submit();	});</script>

					</div><!-- form -->

				<?php endif; ?>
					</div>
				</li>
			</ul>
		</div>


	</div>
</div>