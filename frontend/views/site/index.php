<div class="content">
    <div class="main">
        <div class="wrapper-box module">
            <div class="clear">
                <div class="boxIndent">
                    <div class="box-top">
                        <div class="box-top-bg">
                            <div class="wrapper">
	                            <?=CHtml::image($this->assets."/images/banner.png");?>
                                <div>
	                                <span style="padding-top: 0px"><strong><?=Yii::t('main','Главная страница')?></strong></span>
	                                <!--span style="background: none">Выберите приключение</span-->
                                </div>
                                <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
                            </div>
                        </div>
                    </div> </div>
            </div>
        </div>
        <div class="wrapper">
	        <ul class="list3" style="padding-top: 0px">
		        <h4><?=Yii::t('main','Добро пожаловать на сайт клуба путешественников "Мир Приключений"')?></h4>
		        <iframe width="880" height="480" src="//www.youtube.com/embed/vunm_FPNj84?autoplay=1&rel=0&border=0&showinfo=0&autohide=1&controls=0&modestbranding=1&disablekb=1" frameborder="0" allowfullscreen" frameborder="0" allowfullscreen></iframe>

                <!--span class="lh2"></span-->
                <span style="width:880px; color: #000000; font-size: 18px; line-height: 24px; text-indent: 20px; font-family: Calibri">
                    <?php
                    $image1 = strip_tags($this->widget('common.widgets.fancyPic',['path'=>'team.jpg'], true),'<a><img>');
                    $image2 = strip_tags($this->widget('common.widgets.fancyPic',['path'=>'okno.jpg'], true),'<a><img>');
                    $image3 = strip_tags($this->widget('common.widgets.fancyPic',['path'=>'football3.jpg'], true),'<a><img>');

                    $result = str_replace("{image1}",$image1,$model->html);
                    $result = str_replace('{image2}',$image2,$result);
                    $result = str_replace('{image3}',$image3,$result);
                    ?>
				    <?=$result?>
                </span>
                <!--span class="lh2"></span-->
          </ul>
    </div>
    </div>
</div>
