<li>
	<?=GxHtml::link(Yii::app()->easyImage->thumbOf(STATIC_PATH.$data->image,
		array(
			'resize' => array('width' => 250),
			//	'rotate' => array('degrees' => 90),
			//	'sharpen' => 50,
			//	'background' => '#ffffff',
			'type' => 'jpg',
		),array('class' => 'img-indent')), '/saint/'.$data->id); //array('view', 'id' => $data->id));?>
	<?=GxHtml::link('<h4 style="padding-top:0px; padding-bottom: 0px;">'.GxHtml::encode($data->title).'</h4>', '/saint/'.$data->id); ?>
	<p>
		<b><?=Translate::dates($data->dates)?></b>
		<p><?=($data->days_spent?Yii::t('main','Количество дней').": ".$data->days_spent:'')?></p>
		<?=$data->length?Chtml::tag('span',array(),Yii::t('main','Длительность фильма').": ".Translate::length($data->length)):'';?>
		<?=$data->weight?Chtml::tag('span',array(),Yii::t('main','Размер').": $data->weight"):'';?>
		<?=$data->quality?Chtml::tag('span',array(),Yii::t('main','Качество')." $data->quality"):'';?>
		<p><?=GxHtml::encode($data->description)?></p>
	</p>

	<!--ul class="list">
        <li>
            <!--a href="#"-->
            <?php //echo GxHtml::encode($data->dates); ?>
            <!--/a-->
        <!--/li-->
        <!--li><a href="#">Tuscany Suites &amp; Casino $20</a></li>
        <li><a href="#">South Point Hotel, Casino, and Spa $42</a></li>
        <li><a href="#">The Palms Casino Resort $ 200</a></li>
        <li><a href="#">Hotel Bijou $119</a></li-->
    <!--/ul-->

</li>