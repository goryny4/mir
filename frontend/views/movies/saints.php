<div class="content">
    <div class="main">
        <div class="wrapper-box module">
            <div class="clear">
	            <div class="boxIndent">
		            <div class="box-top">
			            <div class="box-top-bg">
				            <div class="wrapper">
					            <?=CHtml::image($this->assets."/images/banner.png");?>
					            <div>
						            <span style="padding-top: 0px"><strong><?=Yii::t('main','Святыни Православия')?></strong></span>
						            <span style="background: none"><?=Yii::t('main','Православные места и их история')?></span>
					            </div>
                                <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
				            </div>
			            </div>
		            </div> </div>
            </div>
        </div>
        <div class="wrapper">
            <div style="float:left">
                <div id="right">
                    <div class="wrapper-box module">
                        <!--div class="boxTitle">
                            <div class="title">
                                <div class="right-bg">
                                    <div class="left-bg"><h3>Выберите страну</h3></div>
                                </div>
                            </div>
                        </div-->
                        <div class="clear">
                            <div class="boxIndent" style="min-height: 600px">
                                <!--img src="http://images.livedemo00.template-help.com/joomla_35171/images/stories/pages/image4.jpg" alt="alt"-->
                                <div class="indent"><h4><a href="/saint"><?=Yii::t('main','Все страны')?></a></h4> </div>


                                <?php echo $this->widget('frontend.widgets.sidebar',
		                            array(
			                            'entity'=> 'saint',
			                            'selectedCountryId'=> @$selectedCountryId,
		                            ),
		                        true); ?>

                                <!--div class="indent bot-long">
                                    <h4>Океания</h4>
                                    <div class="wrapper">
                                        <ul class="left">
                                            <li><a href="#">Австралия</a></li>
                                            <li><a href="#">Новая Зеландия</a></li>
                                        </ul>

                                    </div>
                                    <p><a href="#"><img src="http://images.livedemo00.template-help.com/joomla_35171/images/stories/image6.png" alt="alt"></a></p>
                                </div-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php

            $this->breadcrumbs = array(
                Movies::label(2),
                Yii::t('app', 'Index'),
            );

            $this->menu = array(
            );
            ?>

            <div id="content">
                <div class="container">
                    <div class="wrapper">
                        <div class="wrapper1-border">
                            <div class="wrapper1-bg clear">
                                <div class="article-text-indent">
                                    <div class="clear">
                                        <table class="contentpaneopen">
                                            <tbody><tr>
                                                <td valign="top">
                                                    <ul class="list2">
														<?php $this->widget('zii.widgets.CListView', array(
                                                            'dataProvider'=>$dataProvider,
                                                            'template'=>'{items}',
                                                            'itemView'=>'_saint',
                                                        )); ?>
                                                        <?php $this->widget('CLinkPager', array(
	                                                        'pages'=>$dataProvider->pagination,
                                                        )); ?>
                                                    <li><?php
													/*
													$dataProvider->getData();
													echo json_encode($dataProvider->totalItemCount);
													*/?></li>
                                                        </ul></td>
                                            </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="article-separator-indent"><span class="article_separator">&nbsp;</span></div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>