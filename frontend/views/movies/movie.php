<?php $this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
);
?>

<div class="content">
	<div class="main">
		<div class="wrapper-box module">
			<div class="clear">
				<div class="boxIndent">
					<div class="box-top">
						<div class="box-top-bg">
							<div class="wrapper">
								<?=CHtml::image($this->assets."/images/banner.png");?>
								<div style="width: 390px">
									<span style="padding-top: 0px"><strong><?=$model->title?></strong></span>
									<span style="background: none"><a style="color:white" href="/movies"><?=Yii::t('main','Видеоролики')?></a></span>
								</div>
                                <?php include(Yii::app()->getBasePath().'/views/layouts/language.php')?>
							</div>
						</div>
					</div> </div>
			</div>
		</div>
		<div class="wrapper">
			<ul class="list3">
				<li class="first">
					<div style="float:left;">
						<?php echo $this->widget('common.widgets.fancyPic',
							array(
								'path'=> $model->image,
								'width'=> 300,
							),
							true); ?>
					</div>
					<div style="float:left; width:550px">
                        <h4 style="padding-top:20px;"><?=GxHtml::encode($model->title)?></h4>
                        <strong><?=Translate::dates($model->dates)?></strong>
						<p><?=($model->days_spent?Yii::t('main','Количество дней').": ".$model->days_spent:'')?></p>
						<ul>
							<?=$model->length?Chtml::tag('p',array(),Yii::t('main','Длительность фильма').": ".Translate::length($model->length)):'';?>
							<?=$model->weight?Chtml::tag('p',array(),Yii::t('main','Размер').": $model->weight"):'';?>
							<?=$model->quality?Chtml::tag('p',array(),Yii::t('main','Качество').": $model->quality"):'';?>
							<p><?=$model->description?></p>
							<p>

							<div class="buttons">
								<div class="wrapper" style="width: 100%; text-align: center">
								<?php if ($model->url) 
											$fullVersion = 'href="/films/'.$model->id.'"';
											else  $fullVersion = ' target="_blank" href="mailto:mir.prikliuchenii@gmail.com?subject=Запрос на доступ к полной версии фильма '.$model->title.'"';
										 ?>
								
									<a <?=$fullVersion?>><?=Yii::t('main','Полная версия фильма')?></a>
									<?php if ($model->forum_url): ?>
										<a target="_blank" href="<?=$model->forum_url?>">Обсудить на Форуме</a>
									<?php endif; ?>
								</div>
							</div>

							</p>
						</ul>

					</div>

				</li>
				<!--div class="alignright"><a class="readon" href="#">Read More</a></div-->
			</ul>




            <ul class="list3" style="padding-top: 0px">
                <?php $i=1; foreach($model->videos as $relatedModel): ?>
                    <li style="padding-top: 20px">
                    <? //=GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('videos/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));?>
						<?php
						$code = Youtube::getCode($relatedModel->youtube_url);
						if (!$relatedModel->name) {
							$url = "http://gdata.youtube.com/feeds/api/videos/". $code;
							$doc = new DOMDocument;
							$doc->load($url);
							$relatedModel->name = str_replace('Мир Приключений - ','',$doc->getElementsByTagName("title")->item(0)->nodeValue);
							$relatedModel->save();
						}
                        $name = (Yii::app()->getLanguage() == 'en'?trim(preg_replace('/[^\00-\255]+/u', '', $relatedModel->name),' .-,!'):$relatedModel->name);
                        //$name = $relatedModel->name;
                        ?>
                        <h4><?=$i++;?>. <?=$name?></h4>
						<!--iframe width="920" height="320" src="//www.youtube-nocookie.com/embed/vwuIA5NeSfY?list=PLTtHUbwXO6_gVN_9qt4WWCMeJDV2lzys1&autoplay=0&rel=0&border=0&showinfo=0&disablekb=1s" frameborder="0" allowfullscreen></iframe-->
						<iframe width="880" height="495" src="//www.youtube.com/embed/<?=$code?>?autoplay=0&rel=0&border=0&showinfo=0&theme=light&color=red&controls=1&autohide=1&disablekb=1&start=26&end=<?=$relatedModel->end?>&modestbranding=1" frameborder="0" allowfullscreen></iframe>
						<?php // Youtube::getPlayer(Youtube::getCode($video->youtube_url),490,array('end'=>$relatedModel->end))?>

					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>