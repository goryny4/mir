<?php $current = Yii::app()->getLanguage();?>
<div style="float:right; padding-right: 200px; width: 120px;">
    <?php foreach (['ru'=>'по-русски','en'=>'in English'] as $key=>$lang) {
        if ($key!=$current) echo CHtml::link(
            Chtml::image($this->assets."/icon/$key.png",'',['style'=>'margin: 0px 6px 8px 8px;']).
            Chtml::tag('p',['style'=>'text-color: white; padding: 0px'],$lang),
            "?lang=$key",['style'=>'text-decoration-style: dashed;']);
    } ?>
</div>