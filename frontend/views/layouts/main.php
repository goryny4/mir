<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="robots" content="index, follow"/>
	<meta name="keywords" content="Mir Prikliuchenii, Мир Приключений, The World of Adventures"/>
	<meta name="description" content="Mir Prikliuchenii, Мир Приключений, The World of Adventures"/>
	<meta name="generator" content="Mir Prikliuchenii, Мир Приключений, The World of Adventures"/>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta name="google-site-verification" content="TfGw891ZhOj80DbUic-lhWPWX2w1ilbIw4g5QyvfZJU" />
	<title><?=Yii::t('main','Мир Приключений')?></title>
	<?php
/*
	$cs = Yii::app()->getClientScript();
	$cs->registerCoreScript('jquery');
	// js
	$cs->registerScriptFile(
	Yii::app()->assetManager->publish(
		Yii::getPathOfAlias('frontend.assets').'/js/jquery.cycle.lite.js',false
	)//, CClientScript::POS_END
	);
	// css
	$cs->registerCssFile(
		Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('frontend.assets').'/css/template.css',false
		)//,CClientScript::POS_END
	);
	$cs->registerCssFile(
		Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('frontend.assets').'/css/background.css',false
		)//,CClientScript::POS_END
	);
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('frontend.assets').'/js/main.js',false)
	//	,CClientScript::POS_END
	);
*/
	$cs = Yii::app()->getClientScript();
	$cs->registerCoreScript('jquery');
	// js
	$cs->registerScriptFile($this->assets.'/js/jquery.cycle.lite.js');
	$cs->registerScriptFile($this->assets.'/js/jquery.lazyload.min.js');
	$cs->registerScriptFile($this->assets.'/js/main.js');

	// css
	$cs->registerCssFile($this->assets.'/css/template18.css');
	$cs->registerCssFile($this->assets.'/css/background.css');
	?>

	<script type="text/javascript">
		var $j=jQuery.noConflict();
		$j(document).ready(function() {
			$j('#slideshow').cycle({
				fx: 'fade',
				pager: '#smallnav',
				pause: 1,
				speed: 10000,
				timeout: 3500
			});
			$j("img.lazy").lazyload({
				 threshold : 1000,
				 effect : "fadeIn"
			});
		});

	</script>
</head>
<body class=" NOTfirst-page">


<div id="slideshow" style="padding-top: 270px">
	<?php //for ($i=1;$i<=5;$i++) {
	//	echo CHtml::image($assets."/img/bg/bg$i.jpg",'',array('class'=>'bgM'));
//	} ?>
	<img src="/bg/bg1.jpg" class="bgM"/>
	<img src="/bg/bg2.jpg" class="bgM"/>
	<!--img src="/bg/bg3.jpg" class="bgM"/-->
	<img src="/bg/bg4.jpg" class="bgM"/>
	<img src="/bg/bg5.jpg" class="bgM"/>
</div>
<div id="wrap">

	<div class="extra">
		<div style="width: 100%; height: 165px; text-align: center; background-color: #000000">
			<div class="main">
				<!-- http://mir-prikliuchenii.com/ -->
                <?php $lang = Yii::app()->getLanguage(); ?>
                <span style="float:left"><a href="/"><?=CHtml::image($this->assets."/logo/mir_black_200.$lang.png");?></a></span>
				<span style="float:left; padding-left: 15px"><a href="/">
                        <?=CHtml::image($this->assets."/logo/club.mir.nea.$lang.png");?>
                    </a></span>
	            <span style="float:right; background-color: white;">
		            <span class="puma" style="float:right;">
		                <a target="_blank" href="http://puma.com"><?=CHtml::image($this->assets."/logo/puma_trans_220_new.$lang.png");?></a>
			        </span>
	            </span>
			</div>
		</div>

		<div class="header">

			<div class="main">
				<div class="nav">
					<?php

					// echo ($this->route);
					$items = array(
						array('label'=>Yii::t('main','В планах'), 'url'=>array('/plans'), 'active'=>(in_array($this->route,array('plans/index','plans/view')))),
						array('label'=>Yii::t('main','Фоторепортажи'), 'url'=>array('/trips'), 'active'=>(in_array($this->route,array('trips/index','trips/view','trips/country')))),
						array('label'=>Yii::t('main','Видеоролики'), 'url'=>array('/movies'), 'active'=>(strpos(Yii::app()->request->requestUri,'movies') || in_array($this->route,array('videos/view')))),
						array('label'=>Yii::t('main','Фильмы'), 'url'=>array('/films'), 'active'=>strpos(Yii::app()->request->requestUri,'film')),
						array('label'=>Yii::t('main','Святыни Православия'), 'url'=>array('/saint'), 'active'=>strpos(Yii::app()->request->requestUri,'saint')),
						array('label'=>Yii::t('main','Члены клуба'), 'url'=>array('/members'), 'active'=>(in_array($this->route,array('members/index'))||(Yii::app()->request->url) == '/members/1')),
						array('label'=>Yii::t('main','Карта'), 'url'=>array('/map'), 'active'=>(in_array($this->route,array('map/index')))),
						array('label'=>Yii::t('main','Контакты'), 'url'=>array('/contact'), 'active'=>(in_array($this->route,array('site/contact'))))
					);
					if (1==2 && $this->route != 'site/index') array_unshift($items,
						array('label'=>Yii::t('main','На главную'), 'url'=>'/', 'active'=>($this->route == 'site/index'))
					);

					$this->widget('zii.widgets.CMenu', array(
						'items'=>$items,
						'htmlOptions'=> array('class' => 'menu sf-menu sf-horizontal sf-shadow'),
					));
					?>

					<div class="clear1"></div>
				</div>
			</div>
		</div>

		<?php echo $content; ?>

		<div class="footer" style="padding-bottom: 20px;">
			<div class="main" style="background-color: #000000">
				<div class="wrapper">
					<h2 style="color:white; padding-top: 10px; padding-bottom: 50px"><?=Yii::t('main','Партнёры клуба')?></h2>
					<span style="float:left;width: 280px; margin-right: 35px">
						<a target="_blank" href="http://puma.com/"><?=CHtml::image(
							$this->assets."/logo/puma_red.jpg"
							,'Puma')?></a>
						</span>
					<span style="float:left;width: 280px; margin-right: 05px"><a href="/pages/3"><?=CHtml::image($this->assets."/logo/wsa.png")?></a></span>
					<span style="float:left;width: 280px;"><a href="http://1001guide.ru/" target="_blank"><?=CHtml::image("/partners/1001guide_logos.jpg")?></a></span>

					<div class="clr"></div>
				</div>
			</div>
			<div class="main">
				<div class="wrapper">
					<p><a href="http://mir-prikliuchenii.com"><?=Yii::t('main','Мир Приключений')?></a> (c) 2015 | <?=Yii::t('main','Клуб путешественников')?>&nbsp; &nbsp; </p>
					<div class="socialmedia" style="text-align: center; width: 350px">
						<span style="float:right"><a rel="nofollow" href="http://www.youtube.com/MirPrikliuchenii" target="_blank"><img class="i48" style="margin:0 8px 0 0;" src="<?=$this->assets."/logo/youtube.jpg"?>" alt="YouTube" title="Follow us on YouTube"/></a></span>
						<span style="float:right"><a rel="publisher" href="http://plus.google.com/105272489406606399768" target="_blank"><img class="i48" style="margin:0 8px 0 0;" src="<?=$this->assets."/logo/plus.png"?>" alt="Google+" title="Follow us on Google+" width="48" height="48"/></a></span>
						<span style="float:right"><a rel="nofollow" href="http://www.facebook.com/MirPrikliuchenii" target="_blank"><img class="i48" style="margin:0 8px 0 0;" src="<?=$this->assets."/logo/facebook.jpg"?>" alt="Facebook" title="Follow us on Facebook"/></a></span>
					</div>
					<div class="clr"></div>
				</div>
			</div>
		</div>

	</div>
	<?php $this->widget('frontend.extensions.fancybox.EFancyBox', array(
		'target'=>'.fancyElm',
		'config'=>array(
			//	'maxWidth'    => 800,
			//	'maxHeight'   => 600,
			'fitToView'   => true,
			'width'       => '90%',
			'height'      => '90%',
			'autoSize'    => true,
			'closeClick'  => true,
			'openEffect'  => 'none',
			'closeEffect' => 'none'
		),
	)); ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-43697533-1', 'mir-prikliuchenii.com');
		ga('send', 'pageview');

	</script>
	<a href="https://plus.google.com/105272489406606399768" rel="publisher"></a>
</body>
</html>