<?php
return [
    'В планах' => 'Our plans',
    'Архив' => 'Past',
    'Фоторепортажи' => 'Photo reports',
    'Видеоролики' => 'Videos',
    'Фильмы' => 'Films',
    'Святыни Православия' => 'Shrines of Orthodoxy',
    'Члены клуба' => 'Club members',
    'Карта' => 'Map',
    'Контакты' => 'Contacts',

    'Мир приключений' => 'The World of Adventures',
    'Мир Приключений' => 'The World of Adventures',
    'Клуб путешественников' => 'Travellers Club',
    'Партнёры клуба' => 'Club partners',
    'Православные места и их история' => 'Orthodox places and its history',
    'На главную' => 'Back to main',

    'География путешествий Валерия Ефимова' => 'The geography of Valerii Efimov trips',

    'Фильм' => 'Film',
    'Фоторепортаж' => 'Photo report',
    'Все страны' => 'All countries',

    'Полная версия фильма' => 'Full version request',
    'Карта путешествия' => 'Map of the journey',
    'Фильмы о наших путешествиях' => 'Films about our trips',
    'Количество дней' => 'Days spent',
    'Длительность фильма' => 'Film duration',
    'Размер' => 'Size',
    'Качество' => 'Quality',
    'Главная страница' => 'Main page',
    'Добро пожаловать на сайт клуба путешественников "Мир Приключений"' => 'Welcome to the website of the travellers club "The World of Adventures"',
    'Расписание наших путешествий' => 'Schedule of our trips',
    'Видеоролики из фильмов о наших путешествиях' => 'Videos from films about our trips',
    'фото с комментариями о наших путешествиях' => 'Photos with comments about our trips',


    'Если у Вас есть какие-либо вопросы или предложения по поводу дальнейших путешествий, пожалуйста, напишите нам.<br/>
						Вы также можете связаться с нами через нашу <a href="http://fb.com/mir.prikliuchenii" target="_blank">группу на Facebook</a>' =>
        'If you have any questions or proposals please contact us using this form.<br/>
						We are also available in our <a href="http://fb.com/mir.prikliuchenii" target="_blank">group on Facebook</a>',
    'Пожалуйста, введите текст на картинке.' => 'Please enter the text on the picture above.',
    'Строчные и прописные буквы равнозначны.' => 'Lower and upper case letters are identical',
    'Отправить сообщение' => 'Send message',
    'Написать нам' => 'Contact us',

    'Ваше имя' => 'Your name',
    'Тема сообщения' => 'Message title',
    'Текст сообщения' => 'Your message',
    'Проверочный код' => 'Verification code',
    'Программа путешествия' => 'The program of the trip',

    'Часть' => 'Part'
];