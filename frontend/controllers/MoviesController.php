<?php

class MoviesController extends EGxController {

    public $layout='//layouts/column2';
    private $lang;
	private $otherLang;
	private $entity;
	
	function __construct($id, $module) {
		foreach (['film','movie','saint'] as $entity) {
			if (strpos(Yii::app()->request->requestUri,$entity) !== false) $this->entity = $entity;
		}
		if (!$this->entity) die('wrong entity');

		parent::__construct($id, $module);
	}

	public function actionView($id) {
		$this->lang = Yii::app()->getLanguage(); $this->otherLang = str_replace($this->lang,'','enru');
		$this->render($this->entity, array(
			'model' => $this->loadModel($id, 'Movies',[
                'select' => "id, active, sorder, dates, url,
                    length, quality, weight, days_spent,
                    COALESCE(title_$this->lang, title_$this->otherLang) as title, COALESCE(description_$this->lang, description_$this->otherLang) as description"
            ])
		));
	}

	public function actionIndex() {
		$this->lang = Yii::app()->getLanguage(); $this->otherLang = str_replace($this->lang,'','enru');
		$criteria = new CDbCriteria;

		$criteria->with = array(($this->entity == 'film')?'videos':'videos2');
		$criteria->together = true;

		$criteria->condition = 'active=1'.(($this->entity == 'film')?' AND url!=""':'').($this->entity == 'saint'?" AND series='saint'":' AND series IS NULL');

		$criteria->order = '`t`.`sorder` DESC';


		$criteria->select = ['id', 'active', 'sorder', 'dates', 'url',
			'length', 'quality', 'weight', 'days_spent',
			"COALESCE(title_$this->lang, title_$this->otherLang) as title",
			"COALESCE(description_$this->lang, description_$this->otherLang) as description"];


        $dataProvider = new CActiveDataProvider('Movies', array(
            'pagination'=>array(
				'pageSize'=>30,
			),

			'criteria'=>$criteria,
			'countCriteria'=>array(
				'condition'=> $criteria->condition
			),

			)
		);
		//die($this->lang);
		$this->render($this->entity.'s', array(
			'dataProvider' => $dataProvider,
		));
	}

	// Movies videos || Films by Country
    public function actionCountry($id) {
		$this->lang = Yii::app()->getLanguage(); $this->otherLang = str_replace($this->lang,'','enru');
		$criteria = new CDbCriteria;

	    $criteria->with = array('moviesCountries');

		$criteria->condition = 'active=1'.(($this->entity == 'film')?' AND url!=""':'').($this->entity == 'saint'?" AND series='saint'":' AND series IS NULL');

		$criteria->order = 'sorder DESC';

	    $criteria->together = true;
	    $criteria->compare('moviesCountries.country_id', $id);
		$criteria->select = ['id', 'active', 'sorder', 'dates', 'url',
			'length', 'quality', 'weight', 'days_spent',
			"COALESCE(title_$this->lang, title_$this->otherLang) as title",
			"COALESCE(description_$this->lang, description_$this->otherLang) as description"];

        $dataProvider=new CActiveDataProvider('Movies', array(
            'criteria'=>$criteria,
            'countCriteria'=>$criteria,
           /* array(
	            'condition'=>'active=1',
	         // 'order' and 'with' clauses have no meaning for the count query
            ),
*/ 			'pagination'=>array(
                'pageSize'=>30,
            ),
        ));

		$this->render($this->entity.'s', array(
			'dataProvider' => $dataProvider,
			'selectedCountryId'=> $id,
		));
	}


}