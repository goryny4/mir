<?php
/**
 *
 * SiteController class
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
class SiteController extends EGxController
{

	public function actionIndex()
	{

/*        $criteria = new CDbCriteria();
        $criteria->order = 'sorder ASC';

        $continents = Continents::model()->findAll($criteria);

/// -----------------------

        foreach ($continents as $continent) {

            $countriesForThisContinent = array();
            foreach ($continent->countries as $country) {
                $countriesForThisContinent[] = array('text' => CHtml::link($country->name, '/trips/index/'.$country->id));
            }

            $tree[] =
                array(
                    'text'     =>$continent->name,
                    'expanded' => true, // будет развернута ветка или нет (по умолчанию)
                    'children' => $countriesForThisContinent
                );
        }

*/
        $lang = Yii::app()->getLanguage(); $other = str_replace($lang,'','enru');
        $model = $this->loadModel(array('alias'=>'about'), 'Pages',[
            'select' => "id, COALESCE(html_$lang, html_$other) as html"
        ]);
        $this->render('index',array(
	        'model' => $model,
	        //    'tree' => $tree,
       ));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if ($model->validate())
			{
				Yii::import('common.components.ses.*');

				$ses = new SimpleEmailService('AKIAJ77RI3DN2DJDWZLA', 'JyvPdTfrr4zqi80+qnzshmxdqgXCxqnt2GtSVUME');
				$ses->enableVerifyPeer(false);
				$ses->enableVerifyHost(false);
				$m = new SimpleEmailServiceMessage();
				$m->addTo('mir.prikliuchenii@gmail.com');
				$m->setFrom('mir.prikliuchenii@gmail.com');//$model->email);
			//	$m->addReplyTo($model->email);
				$m->setSubject($model->subject);
				$m->setMessageFromString($model->body.' - '.$model->name.' ('.$model->email.')');

			//	print_r(
				$ses->sendEmail($m)
		//	)
				;


				Yii::app()->user->setFlash('contact','Спасибо за Ваше сообщение!
				Мы свяжемся с Вами в ближайшее время.');

				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
}