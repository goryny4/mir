<?php

class PagesController extends EGxController {


    public $layout='//layouts/column2';

    public function actionRedir($code = NULL) {
        Yii::app()->request->redirect('http://forum.mir-prikliuchenii.com/discussions', true);
    }

    public function actionView($id) {
        $lang = Yii::app()->getLanguage(); $other = str_replace($lang,'','enru');
        $model = $this->loadModel($id, 'Pages',[
            'select' => "id, alias, COALESCE(title_$lang, title_$other) as title, COALESCE(html_$lang, html_$other) as html"
        ]);

        $this->render('view', array(
            'model' => $model,
        ));
    }

	public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Pages');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Pages('search');
		$model->unsetAttributes();

		if (isset($_GET['Pages']))
			$model->setAttributes($_GET['Pages']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}