<?php

class VideosController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Videos'),
		));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Videos');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}