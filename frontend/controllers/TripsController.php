<?php

class TripsController extends EGxController {

    public $layout='//layouts/column2';

	public function actionView($id) {

        $lang = Yii::app()->getLanguage(); $other = str_replace($lang,'','enru');
        $model = $this->loadModel($id, 'Trips',[
                'select' => "id, active, sorder, dates,
                        COALESCE(title_$lang, title_$other) as title, COALESCE(description_$lang, description_$other) as description,
                        COALESCE(scenario_$lang, scenario_$other) as scenario,
                        plan_id, movie_id"
            ]);

		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionIndex() {
        $lang = Yii::app()->getLanguage(); $other = str_replace($lang,'','enru');
        $dataProvider = new CActiveDataProvider('Trips', array(
				'pagination'=>array(
					'pageSize'=>15,
				),
				'criteria'=>array(
					'condition'=>'active=1',
					'order'=>'sorder DESC',
                    'select'=>"id, active, sorder, dates,
                        COALESCE(title_$lang, title_$other) as title, COALESCE(description_$lang, description_$other) as description,
                        plan_id, movie_id"
				),
				'countCriteria'=>array(
					'condition'=>'active=1',
				),

			)
		);
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}



	public function actionCountry($id) {
		$criteria = new CDbCriteria;

		$criteria->with = array('tripsCountries');

		$criteria->condition = 'active=1';
		$criteria->order = 'sorder DESC';

		$criteria->together = true;
		$criteria->compare('tripsCountries.country_id', $id);

		$dataProvider=new CActiveDataProvider('Trips', array(
			'criteria'=>$criteria,
			'countCriteria'=>$criteria,
			/* array(
					'condition'=>'active=1',
				 // 'order' and 'with' clauses have no meaning for the count query
				),
	*/
			'pagination'=>array(
				'pageSize'=>5,
			),
		));


		$this->render('index', array(
			'dataProvider' => $dataProvider,
			'selectedCountryId'=> $id,
		));
	}

}