<?php

class PlansController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Plans'),
		));
	}

	public function actionIndex() {
        $lang = Yii::app()->getLanguage(); $other = str_replace($lang,'','enru');
        $dataProvider=new CActiveDataProvider('Plans' ,array('criteria'=>array(
			'condition'=>'active=1',
			'order'=>'sorder ASC',
            'select'=>"id, active, sorder, date, image_url,
                        COALESCE(title_$lang, title_$other) as title, COALESCE(description_$lang, description_$other) as description
                        "
            //	,'condition'=>'person_id='.$id,))
		)));

		$criteria=new CDbCriteria;
		$criteria->addInCondition('active',array('-1'));
		$dataProvider2=new CActiveDataProvider('Plans' ,array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sorder DESC',
			),

		));

		$this->render('index', array(
			'dataProvider' => $dataProvider,
			'dataProvider2' => $dataProvider2,
		));
	}

}