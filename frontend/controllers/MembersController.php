<?php

class MembersController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Members'),
		));
	}

	public function actionIndex() {
        $lang = Yii::app()->getLanguage(); $other = str_replace($lang,'','enru');
        $dataProvider = new CActiveDataProvider('Members',array(
		    'pagination'=>array(
                'pageSize'=>20,
            ),
            'criteria'=>array(
                'select'=>"id,
                    COALESCE(name_$lang, name_$other) as name, COALESCE(description_$lang, description_$other) as description,
                    COALESCE(from_$lang, from_$other) as country
                    "
            ),
        ));

		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}