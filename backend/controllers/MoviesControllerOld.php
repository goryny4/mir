<?php

class MoviesController extends EGxController {


    public $layout='//layouts/column2';
    
    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Movies'),
		));
	}

	public function actionCreate() {
		$model = new Movies;

		$this->performAjaxValidation($model, 'movies-form');

		if (isset($_POST['Movies'])) {
			$model->setAttributes($_POST['Movies']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Movies');

		$this->performAjaxValidation($model, 'movies-form');

		if (isset($_POST['Movies'])) {
			$model->setAttributes($_POST['Movies']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Movies')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Movies');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Movies('search');
		$model->unsetAttributes();

		if (isset($_GET['Movies']))
			$model->setAttributes($_GET['Movies']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}