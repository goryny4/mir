<?php

class RoutesController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Routes'),
		));
	}

	public function actionCreate() {
		$model = new Routes;

		$this->performAjaxValidation($model, 'routes-form');

		if (isset($_POST['Routes'])) {
			$model->setAttributes($_POST['Routes']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Routes');

		$this->performAjaxValidation($model, 'routes-form');

		if (isset($_POST['Routes'])) {
			$model->setAttributes($_POST['Routes']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Routes')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Routes');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Routes('search');
		$model->unsetAttributes();

		if (isset($_GET['Routes']))
			$model->setAttributes($_GET['Routes']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}