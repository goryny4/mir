<?php

class ContinentsController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Continents'),
		));
	}

	public function actionCreate() {
		$model = new Continents;

		$this->performAjaxValidation($model, 'continents-form');

		if (isset($_POST['Continents'])) {
			$model->setAttributes($_POST['Continents']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Continents');

		$this->performAjaxValidation($model, 'continents-form');

		if (isset($_POST['Continents'])) {
			$model->setAttributes($_POST['Continents']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Continents')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Continents');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Continents('search');
		$model->unsetAttributes();

		if (isset($_GET['Continents']))
			$model->setAttributes($_GET['Continents']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}