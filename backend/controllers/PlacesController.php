<?php

class PlacesController extends EGxController {


    public $layout='//layouts/column2';

	public function actionCoordinates($id) {
		$model = $this->loadModel($id, 'Places');
		$model->setAttribute('coordinates',$_POST['coordinates']);
		return $model->save(false);
	}

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Places'),
		));
	}

	public function actionCreate() {
		$model = new Places;

        $lastModel = $this->_getLastModel();
        $model->country_id = $lastModel->country_id;
        $model->coordinates = $lastModel->coordinates;
        $model->save(false);
		$this->redirect(array('/places/update/'.$model->id));
	}

    private function _getLastModel() {
        $crit =new CDbCriteria();
        $crit->order = "id DESC";
        $crit->limit = 1;

        return Places::model()->find($crit);
    }

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Places');

	//	$this->performAjaxValidation($model, 'places-form');

		if (isset($_POST['Places'])) {
			$model->setAttributes($_POST['Places']);
			return $model->save(false);
		} else {
			$this->render('update', array(
				'model' => $model
			));
		}
	}

	public function actionUpload($id)
	{
		$model = $this->loadModel($id, 'Places');

		$uploadFolder = STATIC_PATH."places/";//$model->country_id".'.'."$country";
		array_map('unlink', glob("$uploadFolder/$id.*")?:[]); // удаляем предыдущие загрузки для этого места
		if (!is_dir($uploadFolder)) mkdir($uploadFolder, 0777, TRUE);

		Yii::import("ext.EFineUploader.qqFileUploader");
		$uploader = new qqFileUploader();
        // ЭТИ ЖЕ НАСТРОЙКИ НУЖНО ЗАДАТЬ НА СТОРОНЕ КЛИЕНТА
    //    $uploader->allowedExtensions = array('jpg','jpeg','png');//,'nef','tif','tiff','img');
        $uploader->allowedExtensions = array('jpg','jpeg','png','tif','tiff','img');
        $uploader->sizeLimit = 30 * 1024 * 1024;//maximum file size in bytes

		$result = $uploader->handleUpload($uploadFolder);
        if ($n = $uploader->getUploadName()) {
            rename("$uploadFolder/$n","$uploadFolder/$id.$n");
        } else {
            die($result['error']);
        }
		$result['filename'] = $model->id.'.'.$uploader->getUploadName();
		$result['url'] = STATIC_URL;
		$result['folder'] = "places/"; //$model->country_id".'.'."$country";

		$uploadedFile = $uploadFolder.$result['filename'];

		header("Content-Type: text/plain");
		$result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		echo $result;
		Yii::app()->end();
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Places')->delete();
			array_map('unlink', glob(STATIC_PATH."places/$id.*")?:[]); // удаляем предыдущие загрузки для этого места

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Places',array(
			'pagination'=>array(
				'pageSize'=>20,
			)
		));
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Places('search');
		$model->unsetAttributes();
		$dataProvider = new CActiveDataProvider('Places',array(
			'pagination'=>array(
				'pageSize'=>20,
			)
		));
		if (isset($_GET['Places']))
			$model->setAttributes($_GET['Places']);

		$this->render('admin', array(
			'dataProvider' => $dataProvider,
			'model' => $model,
		));
	}

}