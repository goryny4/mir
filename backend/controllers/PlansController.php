<?php

class PlansController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Plans'),
		));
	}

	public function actionCreate() {
		$model = new Plans;


		if (isset($_POST['Plans'])) {
			$model->setAttributes($_POST['Plans']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Plans');


		if (isset($_POST['Plans'])) {
			$model->setAttributes($_POST['Plans']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Plans')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Plans');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Plans('search');
		$model->unsetAttributes();

		if (isset($_GET['Plans']))
			$model->setAttributes($_GET['Plans']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}