<?php

class PagesController extends EGxController {


    public $layout='//layouts/column2';

    public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Pages'),
		));
	}

	public function actionCreate() {
		$model = new Pages;

		$this->performAjaxValidation($model, 'pages-form');

		if (isset($_POST['Pages'])) {
			$model->setAttributes($_POST['Pages']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Pages');

		$this->performAjaxValidation($model, 'pages-form');

		if (isset($_POST['Pages'])) {
			$model->setAttributes($_POST['Pages']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Pages')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Pages');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Pages('search');
		$model->unsetAttributes();

		if (isset($_GET['Pages']))
			$model->setAttributes($_GET['Pages']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}