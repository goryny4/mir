<?php
/**
 * SiteController class
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
class SiteController extends EGxController
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('index'),
                'users'=>array('?'),
            ),
        );
    }

	/**
	 * Renders index
	 */
	public function actionIndex() {
        // echo Yii::app()->user->isGuest ?'Guest':'logged-in';

        /* configure and save gallery model
        $gallery = new Gallery();
        $gallery->name = true;
        $gallery->description = true;
        $gallery->versions = array(
            'small' => array(
                'resize' => array(200, null),
            ),
            'medium' => array(
                'resize' => array(800, null),
            )
        );
        $gallery->save();
*/
		$this->render('index',
            array(
       //         'gallery'=>$gallery,
            )
        );
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


    /**
     * Displays the login page
     */
    public function actionLogin() {

        Yii::import('backend.components.UserIdentity');
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                // $this->redirect(Yii::app()->user->returnUrl);
                Zapier::post(array_merge(['user'=>$_POST['LoginForm']['username']],json_decode(file_get_contents('http://ip-api.com/json/'.Yii::app()->request->getUserHostAddress()),TRUE)));
                $this->redirect(array('/places/admin'));
                Yii::app()->end();
            }
        }
        // display the login form
        //Zapier::post(array_merge(['user'=>'anonymous'],json_decode(file_get_contents('http://ip-api.com/json/'.Yii::app()->request->getUserHostAddress()),TRUE)));
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionConverter() {
        $this->disableProfilers();
        $uploadDir = STATIC_PATH.'/files/';
        $filename = 'converter.exe';
        if( file_exists( $uploadDir.$filename ) ){
            Yii::app()->getRequest()->sendFile( $filename , file_get_contents( $uploadDir.$filename ) , 'application/octet-stream');
        } else die('Файл не найден! '.$uploadDir.$filename );
    //    $this->redirect(Yii::app()->homeUrl);
    }

    public function disableProfilers()
    {
        if (Yii::app()->getComponent('log')) {
            foreach (Yii::app()->getComponent('log')->routes as $route) {
                if (in_array(get_class($route), array('CProfileLogRoute', 'CWebLogRoute', 'YiiDebugToolbarRoute','DbProfileLogRoute'))) {
                    $route->enabled = false;
                }
            }
        }
    }
}
