<?php

class MoviesController extends EGxController
{


    public $layout = '//layouts/column2';

    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Movies'),
        ));
    }

    public function actionCreate()
    {
        $model = new Movies;


        if (isset($_POST['Movies'])) {
            $model->setAttributes($_POST['Movies']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Movies');


        if (isset($_POST['Movies'])) {
            $model->setAttributes($_POST['Movies']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'Movies')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Movies');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin()
    {
        $model = new Movies('search');
        $model->unsetAttributes();

        if (isset($_GET['Movies']))
            $model->setAttributes($_GET['Movies']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }


    public function actionRescan123OldVersionTest()
    {

        foreach ($playlistListFeed as $playlistEntry) {


            $videoFeed = $yt->getPlaylistVideoFeed($playlistEntry->getPlaylistVideoFeedUrl());
            $i = 1;

            echo '<br/><br/><strong>' . $movie->id . ') ' . $videoFeed->getTitle() . "</strong><br/>";

            /**
             * @var Zend_Gdata_YouTube_VideoEntry $videoEntry
             */

            foreach ($videoFeed as $videoEntry) {
                $q = new CDbCriteria();
                $q->addSearchCondition('youtube_url', substr($videoEntry->getFlashPlayerUrl(), 26, 11));
                $video = Videos::model()->find($q);
                if (is_null($video)) $video = new Videos();

                echo '<br/>';
                if ($video->isNewRecord) echo ' ' . $video->sorder = $i++;
                echo ' ' . $video->name = trim(str_replace(array('Мир Приключений - ', 'Мир приключений - '), '', $videoEntry->getTitle()));
                echo ' ' . CHtml::link('ссылка', $video->youtube_url = $videoEntry->getVideoWatchPageUrl());
                echo ' ' . $video->end = $videoEntry->getVideoDuration() - 24;
                $video->movie_id = $movie->id;
                if (!$error = $video->save(false)) {
                    echo "<br/>ОШИБКА ЗАПИСИ!<br/>";
                }

            }
            //	echo $playlistEntry->title->text . "\n";
            //	echo $playlistEntry->description->text . "\n";
        }
    }


    public function actionRescan123()
    {
        Yii::import('vendor.*');

        require_once 'google/apiclient/src/Google/Client.php';
        require_once 'google/apiclient/src/Google/Service/YouTube.php';
        require_once 'google/apiclient/src/Google/Auth/AssertionCredentials.php';

        $key = file_get_contents(Yii::getPathOfAlias('application').'/../API Project-787997783c90.p12');

        $client = new Google_Client();
        $client->setApplicationName("Mir");
        $assertionCredentials = new Google_Auth_AssertionCredentials(
            '364605236788-jol0qhebs1o8keh5fbp6odthvjs3cutl@developer.gserviceaccount.com',[
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/youtube.readonly'
        ], $key, 'notasecret');
        $client->setAssertionCredentials($assertionCredentials);
        $client->setClientId('364605236788-jol0qhebs1o8keh5fbp6odthvjs3cutl.apps.googleusercontent.com');

        $youtube = new Google_Service_YouTube($client);

        $playlists = $youtube->playlists->listPlaylists('id, snippet', [
            'channelId' => 'UCKMeh2DHG8TTEdvCozVUvww',
            'maxResults' => 50,
        ]);

        try {
            $v = $p = 0;
            foreach ($playlists->getItems() as $playlistItem) {
                $p++;
                $movie = Movies::model()->findByAttributes([
                    'playlist_id' => $playlistItem->getId()
                ]);
                if (!$movie) continue;

                $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
                    'playlistId' => $playlistItem->getId(),
                    'maxResults' => 50
                ));
                echo '<br/><br/><strong>' . $movie->id . ') ' . $playlistItem->getSnippet()->title . "</strong><br/>";

                $i = $k = 1;
                foreach ($playlistItemsResponse->getItems() as $video) {
                    $v++;
                    $q = new CDbCriteria();
                    $q->addSearchCondition('youtube_url', $video['snippet']['resourceId']['videoId']);
                    $videoModel = Videos::model()->find($q);
                    if (is_null($videoModel)) $videoModel = new Videos();

                    echo '<br/>';
                    if ($videoModel->isNewRecord) echo '*' . $videoModel->sorder = $i++;
                    else echo ' '.$k++;
                    echo ' ' . $videoModel->name = trim(str_replace(array('Мир Приключений - ', 'Мир приключений - '), '', $video['snippet']['title']));
                    echo ' ' . CHtml::link('ссылка', $videoModel->youtube_url = sprintf('https://www.youtube.com/watch?v=%s&feature=youtube_gdata_player', $video['snippet']['resourceId']['videoId']));
                //    echo ' ' . $videoModel->end = '----';//$video->getVideoDuration() - 24;
                    $videoModel->movie_id = $movie->id;

                    if (!$error = $videoModel->save(false)) {
                        echo "<br/>ОШИБКА ЗАПИСИ!<br/>";
                    }

                }
            }
            echo '<br/>';
            echo '<hr/>';
            echo 'Total found: playlists '.$p.', videos '.$v;

        } catch (Google_Service_Exception $e) {
            echo sprintf('<p>A service error occurred: <code>%s</code></p>',
                htmlspecialchars($e->getMessage()));
        } catch (Google_Exception $e) {
            echo sprintf('<p>An client error occurred: <code>%s</code></p>',
                htmlspecialchars($e->getMessage()));
        }
    }
}