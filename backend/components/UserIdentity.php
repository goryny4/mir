<?php
/**********************************************************************************************
*                            CMS Open Business Card
*                              -----------------
*	version				:	1.1.0
*	copyright			:	(c) 2012 Monoray
*	website				:	http://www.monoray.ru/
*	contact us			:	http://www.monoray.ru/contact
*
* This file is part of CMS Open Business Card
*
* Open Business Card is free software. This work is licensed under a GNU GPL.
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
* Open Business Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* Without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
***********************************************************************************************/



class UserIdentity extends CUserIdentity {

	private $_id;
	public $email;

	public function __construct($email,$password){
		$this->email = $email;
		$this->password = $password;
	}

	public function authenticate() {
		$user = User::model()->find('LOWER(email)=?', array(strtolower($this->email)));

		if ($user === null){
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			return 0;
		}

		if (!$user->validatePassword($this->password)){
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			return 0;
		}
		else {
			$this->_id = $user->id;

			$this->setState('email', $user->email);
			$this->setState('isAdmin', true);

			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode == self::ERROR_NONE;
	}

	public function getId() {
		return $this->_id;
	}

}