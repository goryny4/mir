<?php

class Zapier
{

    public static function post($data)
    {
        if (in_array($data['user'], ['anonymous'])) exit;
        if (in_array($data['isp'], ['Googlebot', 'YANDEX', 'Amazon Technologies'])) exit;

        $url = 'https://hooks.zapier.com/hooks/catch/670792/4z9ft3/';

// use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
    }

}
