<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'plans-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model, 'date', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php $this->widget('ext.tinymce.TinyMce', array(
			'model' => $model,
			'attribute' => 'content',
		)); ?>
		<?php echo $form->error($model,'scenario'); ?>
	</div><!-- row -->

		<div class="row">
		<?php echo $form->labelEx($model,'forum_url'); ?>
		<?php echo $form->textField($model, 'forum_url', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'forum_url'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'image_url'); ?>
		<?php echo $form->textField($model, 'image_url', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'image_url'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->checkBox($model, 'active'); ?>
		<?php echo $form->error($model,'active'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->