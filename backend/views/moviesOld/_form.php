<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'movies-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textArea($model, 'title'); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('videos')); ?></label>
		<?php echo $form->checkBoxList($model, 'videos', GxHtml::encodeEx(GxHtml::listDataEx(Videos::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->