<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sorder')); ?>:
	<?php echo GxHtml::encode($data->sorder); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('title')); ?>:
	<?php echo GxHtml::encode($data->title); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description')); ?>:
	<?php echo GxHtml::encode($data->description); ?>
	<br />

	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('dates')); ?>:
	<?php echo GxHtml::encode($data->dates); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('length')); ?>:
	<?php echo GxHtml::encode($data->length); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('quality')); ?>:
	<?php echo GxHtml::encode($data->quality); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('weight')); ?>:
	<?php echo GxHtml::encode($data->weight); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('playlist_id')); ?>:
	<?php echo GxHtml::encode($data->playlist_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('days_spent')); ?>:
	<?php echo GxHtml::encode($data->days_spent); ?>
	<br />
	*/ ?>

</div>