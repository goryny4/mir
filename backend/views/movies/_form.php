<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'movies-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->checkBox($model, 'active'); ?>
		<?php echo $form->error($model,'active'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sorder'); ?>
		<?php echo $form->textField($model, 'sorder'); ?>
		<?php echo $form->error($model,'sorder'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">

		<?php echo $form->labelEx($model,'forum_url'); ?>
		<?php echo $form->textField($model, 'forum_url', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'forum_url'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dates'); ?>
		<?php echo $form->textField($model, 'dates', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'dates'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'length'); ?>
		<?php echo $form->textField($model, 'length', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'length'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'quality'); ?>
		<?php echo $form->textField($model, 'quality', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'quality'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'weight'); ?>
		<?php echo $form->textField($model, 'weight', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'weight'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'playlist_id'); ?>
		<?php echo $form->textField($model, 'playlist_id', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'playlist_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'days_spent'); ?>
		<?php echo $form->textField($model, 'days_spent'); ?>
		<?php echo $form->error($model,'days_spent'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('trips')); ?></label>
		<?php echo $form->checkBoxList($model, 'trips', GxHtml::encodeEx(GxHtml::listDataEx(Trips::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('videos')); ?></label>
		<?php echo $form->checkBoxList($model, 'videos', GxHtml::encodeEx(GxHtml::listDataEx(Videos::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->