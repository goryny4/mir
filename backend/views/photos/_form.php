<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'photos-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'trip_id'); ?>
		<?php echo $form->dropDownList($model, 'trip_id', GxHtml::listDataEx(Trips::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'trip_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		<?php echo $form->textField($model, 'filename', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'filename'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->