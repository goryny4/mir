<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('trip_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->trip)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('filename')); ?>:
	<?php echo GxHtml::encode($data->filename); ?>
	<br />

</div>