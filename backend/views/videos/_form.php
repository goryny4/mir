<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'videos-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'movie_id'); ?>
		<?php echo $form->dropDownList($model, 'movie_id', GxHtml::listDataEx(Movies::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'movie_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sorder'); ?>
		<?php echo $form->textField($model, 'sorder'); ?>
		<?php echo $form->error($model,'sorder'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'youtube_url'); ?>
		<?php echo $form->textField($model, 'youtube_url', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'youtube_url'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model, 'alias', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'alias'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'end'); ?>
		<?php echo $form->textField($model, 'end'); ?>
		<?php echo $form->error($model,'end'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->