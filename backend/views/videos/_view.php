<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('movie_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->movie)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sorder')); ?>:
	<?php echo GxHtml::encode($data->sorder); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('youtube_url')); ?>:
	<?php echo GxHtml::encode($data->youtube_url); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('alias')); ?>:
	<?php echo GxHtml::encode($data->alias); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('end')); ?>:
	<?php echo GxHtml::encode($data->end); ?>
	<br />

</div>