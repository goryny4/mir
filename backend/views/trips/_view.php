<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sorder')); ?>:
	<?php echo GxHtml::encode($data->sorder); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('title')); ?>:
	<?php echo GxHtml::encode($data->title); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description')); ?>:
	<?php echo GxHtml::encode($data->description); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('youtube_map')); ?>:
	<?php echo GxHtml::encode($data->youtube_map); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dates')); ?>:
	<?php echo GxHtml::encode($data->dates); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('scenario')); ?>:
	<?php echo GxHtml::encode($data->scenario); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('plan_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->plan)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('movie_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->movie)); ?>
	<br />
	*/ ?>

</div>