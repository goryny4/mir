<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'active:boolean',
'sorder',
'title',
'description',
'youtube_map',
'dates',
'scenario',
array(
			'name' => 'plan',
			'type' => 'raw',
			'value' => $model->plan !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->plan)), array('plans/view', 'id' => GxActiveRecord::extractPkValue($model->plan, true))) : null,
			),
array(
			'name' => 'movie',
			'type' => 'raw',
			'value' => $model->movie !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->movie)), array('movies/view', 'id' => GxActiveRecord::extractPkValue($model->movie, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('tripsCountries')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->tripsCountries as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('tripsCountries/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('countries')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->countries as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('countries/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>