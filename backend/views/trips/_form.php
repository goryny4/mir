<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'trips-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->checkBox($model, 'active'); ?>
		<?php echo $form->error($model,'active'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sorder'); ?>
		<?php echo $form->textField($model, 'sorder'); ?>
		<?php echo $form->error($model,'sorder'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'youtube_map'); ?>
		<?php echo $form->textField($model, 'youtube_map', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'youtube_map'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dates'); ?>
		<?php echo $form->textField($model, 'dates', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'dates'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'scenario'); ?>
		<?php echo $form->textArea($model, 'scenario'); ?>
		<?php echo $form->error($model,'scenario'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'plan_id'); ?>
		<?php echo $form->dropDownList($model, 'plan_id', GxHtml::listDataEx(Plans::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'plan_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'movie_id'); ?>
		<?php echo $form->dropDownList($model, 'movie_id', GxHtml::listDataEx(Movies::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'movie_id'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('tripsCountries')); ?></label>
		<?php echo $form->checkBoxList($model, 'tripsCountries', GxHtml::encodeEx(GxHtml::listDataEx(TripsCountries::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('countries')); ?></label>
		<?php echo $form->checkBoxList($model, 'countries', GxHtml::encodeEx(GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->