<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'routes-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'trip_id'); ?>
		<?php echo $form->dropDownList($model, 'trip_id', GxHtml::listDataEx(Trips::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'trip_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'google_maps_url'); ?>
		<?php echo $form->textField($model, 'google_maps_url', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'google_maps_url'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->