<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('trip_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->trip)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('google_maps_url')); ?>:
	<?php echo GxHtml::encode($data->google_maps_url); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description')); ?>:
	<?php echo GxHtml::encode($data->description); ?>
	<br />

</div>