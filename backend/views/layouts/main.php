<?php
/**
 * main.php layout
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<?php
//	$cs = Yii::app()->getClientScript();
//	$cs->registerCoreScript('jquery');

	// js
	/*	$cs->registerScriptFile($this->assets.'/js/jquery.cycle.lite.js');
		$cs->registerScriptFile($this->assets.'/js/main.js');
		// css
		$cs->registerCssFile($this->assets.'/css/template7.css');
		$cs->registerCssFile($this->assets.'/css/background.css'); */
?>

	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {

		});
	</script>
	<style>
		body {
			padding-bottom: 40px;
		}
	</style>

	<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">


    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
	improve your experience.</p>
<![endif]-->


<?php

    // array('label' => 'Home', 'url' => '#', 'active' => true),
    $items = array();
    if (!Yii::app()->user->isGuest) $items[] = array('label' => 'Logout', 'url' => '/site/logout'
    );

    $this->widget('bootstrap.widgets.TbNavbar', array(
    'collapseOptions' => array('style'=>'float:right'),

    'brandLabel' => 'Мир Приключений',
    'brandUrl' => '/',
    'color' => TbHtml::NAVBAR_COLOR_INVERSE,
    'collapse' => true, // requires bootstrap-responsive.css
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbNav',
            'items' => $items,
        ),
    ),
)); ?>
<div class="container" style="margin-top:45px">
    <?php if (!Yii::app()->user->isGuest): ?>
    <!-- Example row of columns -->
    <div class="row">
		<?php if (Yii::app()->user->id == 1): // Егор?>
        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbNav', array(
                'type' => TbHtml::NAV_TYPE_PILLS,
                'stacked' => true,
                'items' => array(
                    array('label' => 'Континенты', 'url' => array('/continents')),
                    array('label' => '- Страны', 'url' => array('/countries')),
                    array('label' => '- - Места', 'url' => array('/places')),
                ),
            )); ?>
        </div>


        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbNav', array(
                'type' => TbHtml::NAV_TYPE_PILLS,
                'stacked' => true,
                'items' => array(
	                array('label' => 'Путешествия', 'url' => array('/trips')),
	                array('label' => 'Планы', 'url' => array('/plans')),
                ),
            )); ?>
        </div>

        <div class="span3">
           <?php $this->widget('bootstrap.widgets.TbNav', array(
                'type' => TbHtml::NAV_TYPE_PILLS,
                'stacked' => true,
                'items' => array(
                    array('label' => 'Фильмы', 'url' => array('/movies')),
                    array('label' => '- Ролики на Youtube', 'url' => array('/videos')),
                ),
            )); ?>
        </div>

        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbNav', array(
                'type' => TbHtml::NAV_TYPE_PILLS,
                'stacked' => true,
                'items' => array(
                   // array('label' => 'Карта', 'url' => array('/map')),
	                array('label' => 'О нас (Главная)', 'url' => array('/pages/1')),
	                array('label' => 'Участники', 'url' => array('/pages/2')),
                ),
            )); ?>
        </div>
		<?php endif; ?>

<!-- ---------------------------- EF ------------------------------- -->
		<?php if (Yii::app()->user->id == 2): ?>
		<div class="span3">
			<?php $this->widget('bootstrap.widgets.TbNav', array(
				'type' => TbHtml::NAV_TYPE_PILLS,
				'stacked' => true,
				'items' => array(
					array('label' => '- Страны', 'url' => array('/countries')),
					array('label' => '- - Места', 'url' => array('/places/admin')),
				),
			)); ?>
		</div>
		<?php endif; ?>
<!-- ---------------------------- TRANSLATOR ------------------------------- -->
        <?php if (Yii::app()->user->id == 3): ?>
            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbNav', array(
                    'type' => TbHtml::NAV_TYPE_PILLS,
                    'stacked' => true,
                    'items' => array(
                        array('label' => 'Места', 'url' => array('/places')),
                    ),
                )); ?>
            </div>


            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbNav', array(
                    'type' => TbHtml::NAV_TYPE_PILLS,
                    'stacked' => true,
                    'items' => array(
                        array('label' => 'Путешествия', 'url' => array('/trips')),
                        array('label' => 'Планы', 'url' => array('/plans')),
                    ),
                )); ?>
            </div>

            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbNav', array(
                    'type' => TbHtml::NAV_TYPE_PILLS,
                    'stacked' => true,
                    'items' => array(
                        array('label' => 'Фильмы', 'url' => array('/movies')),
                    ),
                )); ?>
            </div>

            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbNav', array(
                    'type' => TbHtml::NAV_TYPE_PILLS,
                    'stacked' => true,
                    'items' => array(
                        // array('label' => 'Карта', 'url' => array('/map')),
                        array('label' => 'О нас (Главная)', 'url' => array('/pages/1')),
                        array('label' => 'Участники', 'url' => array('/pages/2')),
                    ),
                )); ?>
            </div>
        <?php endif; ?>

		</div>

    <hr/>
<?php endif; ?>


    <?php echo $content; ?>
</div>

</body>
</html>