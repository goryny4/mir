<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Список мест'), 'url' => array('admin')),
	array('label'=>Yii::t('app', 'Добавить место') , 'url' => array('create')),
);
?>

<h1><?php echo Yii::t('app', '') . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
<a style="float: right" href="/places/update/<?=$model->id?>">Редактировать это место</a>
<div id="list" style="float:left;">
	<div id="efine_name" style="float:right">
		<?php if ($img = $model->getImage()) echo $this->widget('common.widgets.fancyPic',
			array(
				'path'=> $img,
				'width'=> 200,
			),
			true); ?>
	</div>
    <div style="float: left;  width: 500px">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data' => $model,
			'attributes' => array(
		'id',
				'name',
				'description',
		array(
					'name' => 'country',
					'type' => 'raw',
					'value' => $model->country !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->country)), array('countries/view', 'id' => GxActiveRecord::extractPkValue($model->country, true))) : null,
					),
			),
		)); ?>
	</div>
    <div style="float: left;  width: 700px">
        <?php // -------------- КАРТА --------------------
        Yii::import('common.extensions.egmap.*');


        //$iconPath = $this->assets."/icon/palace-2.png";
        $iconPath = $this->assets."/icon/world-marker.png";

        $icon = new EGMapMarkerImage($iconPath);
        $icon->setSize(24, 23);
        $icon->setAnchor(16, 16.5);
        $icon->setOrigin(0, 0);

        $gMap = new EGMap();
        $gMap->zoom = 6;
        $mapTypeControlOptions = array(
            'position'=> EGMapControlPosition::LEFT_BOTTOM,
            'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
        );
        $gMap->mapTypeControlOptions= $mapTypeControlOptions;

        if ($model) {
            $coordinates = isset($model->coordinates)?explode(', ',$model->coordinates):array('38.348850','-0.477551');
            $marker = new EGMapMarker($coordinates[0], $coordinates[1], array('title' => 'Место',
                'icon' => $icon, 'draggable' => false), 'marker');
            //	$marker->addHtmlInfoWindow($info_window_a);
            $gMap->addMarker($marker);
            $gMap->setCenter($coordinates[0], $coordinates[1]);
        }

        $gMap->setWidth(700);

        $gMap->renderMap();
        ?>
    </div>
</div>


