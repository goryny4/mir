<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'places-form',
	'enableAjaxValidation' => true,
));
?>

	<div style="width: 700px; float: left">
		<a style="float: right" href="/places/view/<?=$model->id?>">Просмотреть это место</a>
	</div>
	<?php echo $form->errorSummary($model); ?>

	<div class="row" style="width: 500px; float: left">
		<?php echo $form->labelEx($model,'Название места'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div><!-- row -->
	<div id="efine_name" style="float: right">
		<?php if ($img = $model->getImage()) echo $this->widget('common.widgets.fancyPic',
			array(
				'path'=> $img,
				'width'=> 200,
			),
			true); ?>
	</div>

	<div class="row" style="width: 500px; float: left">
		<?php echo $form->labelEx($model,'Описание'); ?>
		<?php echo $form->textArea($model, 'description',array('style' => 'width:400px')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div><!-- row -->

	<div class="row" style="float: right">
		<?php $this->widget('common.extensions.EFineUploader.EFineUploader',
			array(
				'id'=>'FineUploader',
				'config'=>array(
					'autoUpload'=>true,
					'request'=>array(
						'endpoint'=> $this->createUrl("/places/upload/$model->id"),
						'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken),
					),
					'retry'=>array('enableAuto'=>true,'preventRetryResponseProperty'=>true),
					'chunking'=>array('enable'=>false,'partSize'=>100),//bytes
					'callbacks'=>array(
						'onComplete'=>"js:function(id, name, response){ // for test purpose
						 	$('#efine_name').html('<img width=\"200\" src=\"' + response.url + response.folder + '/' + response.filename + '\"/>');
					   }",
						//'onError'=>"js:function(id, name, errorReason){ }",
						'onValidateBatch' => "js:function(fileOrBlobData) {}", // because of crash
						//'onComplete'=>"js:function(id, name, response){  }",
						//'onError'=>"js:function(id, name, errorReason){ }",
					),
                    // ЭТИ ЖЕ НАСТРОЙКИ НУЖНО ЗАДАТЬ НА СТОРОНЕ СЕРВЕРА
					'validation'=>array(
                    //    'allowedExtensions'=>array('jpg','jpeg','png'),
                        'allowedExtensions'=>array('jpg','jpeg','png', 'tif','tiff', 'img'),
                        'sizeLimit'=>30 * 1024 * 1024,//maximum file size in bytes
						'minSizeLimit'=>128*1024,// minimum file size in bytes
					),
				)
			));
		?>
	</div>
	<div class="row" style="width: 500px; float: left">
		<?php echo $form->labelEx($model,'Страна');
		$crit = new CDbCriteria();
		$crit->order = 'name ASC';
		?>
		<?=$form->dropDownList($model, 'country_id', GxHtml::listDataEx(Countries::model()->findAll($crit))); ?>
		<?php echo $form->error($model,'country_id'); ?>
	</div><!-- row -->

	<?php
//	echo GxHtml::submitButton(Yii::t('app', 'Сохранить данные о месте'),array('style'=>'float:right; margin-top: 20px'));
	$this->endWidget();
	?>
<div style="width: 700px; float: left">
	<?php // -------------- КАРТА --------------------
	Yii::import('common.extensions.egmap.*');

	//$iconPath = $this->assets."/icon/palace-2.png";
	$iconPath = $this->assets."/icon/world-marker.png";

	$icon = new EGMapMarkerImage($iconPath);
	$icon->setSize(24, 23);
	$icon->setAnchor(16, 16.5);
	$icon->setOrigin(0, 0);

	$gMap = new EGMap();
	$mapTypeControlOptions = array(
		'position'=> EGMapControlPosition::LEFT_BOTTOM,
		'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
	);
	$gMap->mapTypeControlOptions= $mapTypeControlOptions;


	// Saving coordinates after user dragged our marker.
	$dragevent = new EGMapEvent('dragend', "function (event) { $.ajax({
						'type':'POST',
						'url':'".$this->createUrl('places/coordinates/'.$model->id)."',
						'data':({'coordinates': event.latLng.lat() + ', ' + event.latLng.lng()}),
						'cache':false,
					});}", false, EGMapEvent::TYPE_EVENT_DEFAULT);
	// ---- INFO BOX ------------
/*
	$info_box = new EGMapInfoBox('<div style="color:#fff;border: 1px solid black; margin-top: 8px; background: #000; padding: 5px;">I am a marker with info box!</div>');

	// set the properties
	$info_box->pixelOffset = new EGMapSize('0','-140');
	$info_box->maxWidth = 0;
	$info_box->boxStyle = array(
		'width'=>'"280px"',
		'height'=>'"120px"',
		'background'=>'"url(http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/examples/tipbox.gif) no-repeat"'
	);
	$info_box->closeBoxMargin = '"10px 2px 2px 2px"';
	$info_box->infoBoxClearance = new EGMapSize(1,1);
	$info_box->enableEventPropagation ='"floatPane"';

	$marker = new EGMapMarker($centerDolgota, $centerShirota, array('title' => 'Marker With Info Box'));
	$marker->addHtmlInfoBox($info_box);
	$gMap->addMarker($marker);
*/
	//       $icon = new EGMapMarkerImage("http://google-maps-icons.googlecode.com/files/gazstation.png");
    $coordinates = isset($model->coordinates)?explode(', ',$model->coordinates):array('38.348850','-0.477551');
    $gMap->setCenter($coordinates[0], $coordinates[1]);
    $gMap->zoom = 6;
    if ($model) {
        $marker = new EGMapMarker($coordinates[0], $coordinates[1], array('title' => 'Место',
			'icon' => $icon, 'draggable' => true), 'marker', array('dragevent' => $dragevent));
	//	$marker->addHtmlInfoWindow($info_window_a);
		$gMap->addMarker($marker);

		// If we don't have marker in database - make sure user can create one
	} else {
        // Setting up new event for user click on map, so marker will be created on place and respectful event added.
		$gMap->addEvent(new EGMapEvent('click',
			'function (event) {var marker = new google.maps.Marker({position: event.latLng, map: ' . $gMap->getJsName() .
			', draggable: true, icon: ' . $icon->toJs() . '}); ' . $gMap->getJsName() .
			'.setCenter(event.latLng); var dragevent = ' . $dragevent->toJs('marker') .
			'; $.ajax({' .
			'"type":"POST",' .
			'"url":"' . $this->createUrl('places/coordinates') . '/1",' .
			'"data":({"coordinates": event.latLng.lat() + \', \'+ event.latLng.lng()}),' .
			'"cache":false,' .
			'}); }', false, EGMapEvent::TYPE_EVENT_DEFAULT_ONCE));
	}

    $gMap->setWidth(700);
	$gMap->renderMap();
	?>
</div>
</div><!-- form -->