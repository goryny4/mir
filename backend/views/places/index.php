<?php

$this->breadcrumbs = array(
	Places::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Список мест'), 'url' => array('admin')),
	array('label'=>Yii::t('app', 'Добавить место') , 'url' => array('create')),
);
?>

<h1><?php echo GxHtml::encode(Places::label(2)); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
//	'itemView'=>'_view',
)); 