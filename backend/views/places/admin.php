<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Редактирование мест'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Список мест'), 'url'=>array('admin')),
		array('label'=>Yii::t('app', 'Добавить место'), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('places-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Список мест'); ?></h1>
<a style="float: right" href="/places/create">Добавить новое место</a>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display: none">
	<p>
		You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
	</p>

	<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'places-grid',
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'columns' => array(
        'id',
        array(
            'type' => 'raw',
            'name'  =>  'image',
            'value' =>  '$data->getImageInWidget()',
            'sortable' => true
        ),
        'name',
		array(
				'name'=>'country_id',
				'value'=>'GxHtml::valueEx($data->country)',
				'filter'=>GxHtml::listDataEx(Countries::model()->findAllAttributes(null, true)),
				),
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>