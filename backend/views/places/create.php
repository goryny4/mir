<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Список мест'), 'url' => array('admin')),
	array('label'=>Yii::t('app', 'Добавить место') , 'url' => array('create')),
);
?>

<h1><?php echo Yii::t('app', 'Добавление нового места'); ?></h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model,
//		'buttons' => 'create'
));
?>