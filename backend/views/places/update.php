<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);

$this->menu = array(
//	array('label' => Yii::t('app', 'Просмотр места'), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
	array('label' => Yii::t('app', 'Список мест'), 'url'=>array('admin')),
	array('label' => Yii::t('app', 'Добавить место'), 'url'=>array('create')),
);
?>

<h1><?php echo Yii::t('app', 'Редактирование ' . GxHtml::encode(GxHtml::valueEx($model))); ?></h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model));
?>