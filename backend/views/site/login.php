<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'htmlOptions'=>array('class'=>'well'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->textField($model, 'username', array('class'=>'span3'));?>
	<?php echo $form->passwordField($model, 'password', array('class'=>'span3'));?>
	<?php echo $form->checkBox($model, 'rememberMe');?>

	<div class="form-actions">
		<?php $this->widget('zii.widgets.jui.CJuiButton', array('buttonType'=>'submit',//'type'=>'primary',
            'caption'=>'Войти',
		    'name'=>'Submit'
        ));?>
		<?php /*$this->widget('zii.widgets.jui.CJuiButton', array('buttonType'=>'reset',
       // 'label'=>'Reset'
            'name'=>'Reset'
        )); */ ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
