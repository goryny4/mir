<?php

class EHtml
{
	static function ul($array) {
		foreach (array_filter($array) as $label=>$value) {
			$result = Chtml::tag('li','',Chtml::tag('span',array(),$label).': '.$value);
		}
		return Chtml::tag('ul',array(),$result);
	}

}
