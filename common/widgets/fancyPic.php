<?php

class fancyPic extends CWidget
{
	public $path;
	public $width;
	public $height;
	public $quality;
	public $class;
	public $lazy;

	public function init()
	{
		$this->checkAndDownload();

		$this->render('fancyPic',array(
			'path' => STATIC_PATH.$this->path,
			'url' => STATIC_URL.$this->path,
			'width' => ($this->width?:880),
			'height' => ($this->height?:660),
			'quality' => ($this->quality?:80),
			'class' => ($this->class?:'tripImage'),
			'lazy'=> ($this->lazy?:false)
		));
	}


	function checkAndDownload() {
		if (is_file(STATIC_PATH.$this->path) && filesize(STATIC_PATH.$this->path)) return;

		//	throw new Exception('ОШИБКА В ИМЕНИ ФАЙЛА: '.STATIC_PATH.$this->path,E_USER_ERROR);
		set_time_limit(0);

		$url = 'http://static.mir-prikliuchenii.com/' . $this->path;
		$fp = fopen(STATIC_PATH . $this->path, 'w+');

		$ch = curl_init($url);

		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_BINARYTRANSFER => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FILE => $fp,
			CURLOPT_TIMEOUT => 50,
			//		CURLOPT_USERAGENT => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
			CURLOPT_FOLLOWLOCATION => true
		));

		$results = curl_exec($ch);
		if (curl_exec($ch) === false) echo 'Curl error: ' . curl_error($ch);
		fclose($fp);
	}
}