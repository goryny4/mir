<?php

class magicReplace extends CWidget {

	public $scenario;
	public $photos;

	function init() {

		// ищем фото в тексте
		preg_match_all ('/\(*фото(.*?)\)/', $this->scenario, $matches);
		foreach ($matches[0] as $key=>$value) {
			$matchNums = array();
			$preNums = explode(',',$matches[1][$key]);
			// проходимся по элементам типа 13, 14, 15, 16-19, 55
			foreach ($preNums as $pn) {
				$matchElementNumbers = explode('-',$pn);
				if (count($matchElementNumbers)==2) {
					for ($i = (int)$matchElementNumbers[0];$i<=(int)$matchElementNumbers[1];$i++) $matchNums[] = $i;
				} else $matchNums[] = trim($matchElementNumbers[0]);
			}
			$photos = '';
			foreach ($matchNums as $num) if (isset($this->photos[trim((int)$num)])) {
				$photos .= $this->widget('common.widgets.fancyPic',
					array(
						'path'=> $this->photos[trim((int)$num)],
						'lazy'=>true
					),
				true);
			}

			if ($value[0]!='(') $value = '( '.$value;
			$this->scenario = str_replace($value,$photos,$this->scenario);

		}

		// ищем видео в тексте
		preg_match_all ('/\(*видео (.*?)\)/', $this->scenario, $matches);
		foreach ($matches[0] as $key=>$value) {
			$alias = explode(',',$matches[1][$key]);
			$seconds = array();
			if ($video = Videos::model()->findByAttributes(array('alias'=>$alias[0]))) {
				if ($value[0]!='(') $value = '( '.$value;
				if (isset($alias[1])) $seconds['start'] = $alias[1];
				if (isset($alias[2])) { $seconds['end'] = $alias[2]; } elseif ($video->end > 0) $seconds['end'] = $video->end;
				$this->scenario = str_replace($value,'<p>'.Youtube::getPlayer($video->youtube_url,500,$seconds).'</p>',$this->scenario);
			}
		}

// ищем видео в тексте
		preg_match_all ('/\(*video (.*?)\)/', $this->scenario, $matches);
		foreach ($matches[0] as $key=>$value) {
			$alias = explode(',',$matches[1][$key]);
			$seconds = array();
			if ($video = Videos::model()->findByAttributes(array('alias'=>$alias[0]))) {
				if ($value[0]!='(') $value = '( '.$value;
				if (isset($alias[1])) $seconds['start'] = $alias[1];
				if (isset($alias[2])) { $seconds['end'] = $alias[2]; } elseif ($video->end > 0) $seconds['end'] = $video->end;
				$this->scenario = str_replace($value,'<p>'.Youtube::getPlayer($video->youtube_url,490,$seconds).'</p>',$this->scenario);
			}
		}

// ищем видео в тексте
		preg_match_all ('/\(*Video (.*?)\)/', $this->scenario, $matches);
		foreach ($matches[0] as $key=>$value) {
			$alias = explode(',',$matches[1][$key]);
			$seconds = array();
			if ($video = Videos::model()->findByAttributes(array('alias'=>$alias[0]))) {
				if ($value[0]!='(') $value = '( '.$value;
				if (isset($alias[1])) $seconds['start'] = $alias[1];
				if (isset($alias[2])) { $seconds['end'] = $alias[2]; } elseif ($video->end > 0) $seconds['end'] = $video->end;
				$this->scenario = str_replace($value,'<p>'.Youtube::getPlayer($video->youtube_url,490,$seconds).'</p>',$this->scenario);
			}
		}

// удаляем всё, что осталось в скобках
		preg_match_all ('/\((.*?)\)/', $this->scenario, $matches);
		foreach ($matches[0] as $key=>$value) {
			$this->scenario = str_replace($value,'<span style="display:none">ОШИБКА РАСПОЗНАВАНИЯ: '.$value.'</span>',$this->scenario);
		}


		$this->render('magicReplace');
	}
}