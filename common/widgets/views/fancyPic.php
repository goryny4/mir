<p><a class="fancyElm" href="<?=$url?>">
	<?php $htmlOptions = array('class' => $class.($lazy?' lazy':''));
	$params = array(
		'resize' => array('width' => $width,'height'=>$height),
		//	'rotate' => array('degrees' => 90),
		//	'sharpen' => 50,
		//	'background' => '#ffffff',
		'type' => 'jpg',
		'quality' => $quality,
	);
	if ($lazy) $htmlOptions['data-original'] = Yii::app()->easyImage->thumbSrcOf($path,$params);?>
	<?=Yii::app()->easyImage->thumbOf($lazy?$path:$path,$params,$htmlOptions)?>
</a></p>
