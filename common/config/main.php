<?php
/**
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

return array(
	'language'=>'ru',
	'sourceLanguage'=>'ru',
  	'name' => '{APPLICATION NAME}',
	'preload' => array('log','debug'),
	'aliases' => array(
		'frontend' => dirname(__FILE__) . '/../..' . '/frontend',
		'common' => dirname(__FILE__) . '/../..' . '/common',
		'backend' => dirname(__FILE__) . '/../..' . '/backend',
		'vendor' => dirname(__FILE__) . '/../..' . '/common/lib/vendor',
        // EGOR
        'bootstrap' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiistrap', // change this if necessary
        'yiiwheels' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiiwheels', // change this if necessary

        'ext' => dirname(__FILE__) . '/../..' . '/common/extensions/'
    ),
    'import' => array(
        'ext.components.*',
		'common.components.*',
		'common.helpers.*',
		'common.models.*',
		'application.controllers.*',
		'application.extensions.*',
		'application.helpers.*',
		'application.models.*',
		'vendor.2amigos.yiistrap.helpers.*',
		'vendor.2amigos.yiiwheels.helpers.*',
        // EGOR
        'ext.giix-components.*', // giix components
        'bootstrap.helpers.TbHtml',

 //       'ext.GalleryManager.models.*',
 //       'vendor.z_bodya.yii-image.Image',

//        'ext.fileimagebehavior.*',
	    'ext.easyimage.EasyImage',
	 //   'ext.Yiippod.Yiippod'
    ),
	'components' => array(

		'debug' => array(
			'class' => 'vendor.zhuravljov.yii2-debug.Yii2Debug',
			'showConfig' => true,
		),

		'image'=>array(
    //        'class'=>'vendor.z_bodya.yii-image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
            // ImageMagick setup path
        //    'params'=>array('directory'=>'D:/Program Files/ImageMagick-6.4.8-Q16'),
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',
        ),
        // yiiwheels configuration
        'yiiwheels' => array(
            'class' => 'yiiwheels.YiiWheels',
        ),
		'db'=>array(
		//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		'easyImage' => array(
			'class' => 'common.extensions.easyimage.EasyImage',
			//'driver' => 'GD',
			//'quality' => 100,
			//'cachePath' => '/assets/easyimage/',
			//'cacheTime' => 2592000,
			//'retinaSupport' => false,
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log' => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'        => 'CDbLogRoute',
					'connectionID' => 'db',
					'levels'       => 'error, warning',
				),
			),
		),
	),
	// application parameters
	'params' => array(
		// php configuration
		'php.defaultCharset' => 'utf-8',
		'php.timezone'       => 'UTC',
		'adminEmail' => 'egoryny4@gmail.com'
	),
);
