<?php

class EgorActiveRecord extends EActiveRecord
{
	public function getImage() {
		$entityDir = STATIC_PATH.strtolower(get_class($this));

		if (is_dir($entityDir) || mkdir($entityDir)) {
			if ($dh = opendir($entityDir)) {
				while (($file = readdir($dh)) !== false) {
					if (is_file($entityDir.'/'.$file)) {
						switch ((int)$file) {
							case $this->id:
								$result = strtolower(get_class($this)).'/'.$file;
								break;
							case 0: $default = strtolower(get_class($this)).'/'.$file;
						}
					}
				}
			}
		}
		if (!isset($result)) {
			$result = isset($default)?$default:'default.jpg';
		}

		return $result;
	}


	public function getPhotos() {
		$entityDir = STATIC_PATH.'/'.strtolower(get_class($this));
		$photos = array();

		if (is_dir($entityDir) || mkdir($entityDir)) {
			if ($dh = opendir($entityDir)) {
				while (($file = readdir($dh)) !== false) {
					if (is_dir($entityDir.'/'.$file)) {
						if ((int)$file == $this->id) {
							if ($dh2 = opendir($entityDir.'/'.$file.'/photos/')) {
								while (($file2 = readdir($dh2)) !== false) {
									if (in_array($file2,array('.','..'))) continue;

									//	$file = iconv( "iso-8859-1", "utf-8", $file);
									$file2 = str_replace('à','а',utf8_encode($file2));
									preg_match('/[\d]+/',$file2,$matches);
									if (count($matches)) $photos[$matches[0]]	= strtolower(get_class($this)).'/'.$file.'/photos/'.$file2;
								}
								closedir($dh2);
							}
							break;
						}
					}
				}
				closedir($dh);
			}
		}

		return $photos;
	}

}
