<?php

class EGxController extends CController{
    public $meta_keywords = array();
    public $meta_description = array();
    public $breadcrumbs;

    // ADDED BY EGOR
    public $menu;

    function init()
    {
        parent::init();

        if ($lang = Yii::app()->getRequest()->getParam('lang')) {
            Yii::app()->session['lang'] = in_array($lang,['ru','en'])?$lang:'ru';
        }

        Yii::app()->language = (isset(Yii::app()->session['lang'])?Yii::app()->session['lang']:'ru');
    }

    /**
     * Gets a param
     * @param $name
     * @param null $defaultValue
     * @return mixed
     */
    public function getActionParam($name, $defaultValue = null)
    {
        return Yii::app()->request->getParam($name, $defaultValue );
    }

    /**
     * Loads the requested data model.
     * @param string the model class name
     * @param integer the model ID
     * @param array additional search criteria
     * @param boolean whether to throw exception if the model is not found. Defaults to true.
     * @return CActiveRecord the model instance.
     * @throws CHttpException if the model cannot be found
     */

    public function loadModel($key, $modelClass, $criteria = array(), $exceptionOnNull = true)
    {
        // EController block

        if (!empty($criteria)) {
            $finder = CActiveRecord::model($modelClass);
            $c = new CDbCriteria($criteria);
            if (!is_array($key)) {
                $c->mergeWith(array(
                    'condition' => $finder->tableSchema->primaryKey . '=:id',
                    'params' => array(':id' => $key),
                ));
            } else {
                $c->mergeWith(array(
                    'condition' => key($key). '=:value',
                    'params' => array(':value' => current($key)),
                ));
            }
            $model = $finder->find($c);

            if (isset($model))
                return $model;
            else if ($exceptionOnNull)
                throw new CHttpException(404, 'Unable to find the requested object.');
        }

        // ------------ // ------------------------

        // Get the static model.
        $staticModel = GxActiveRecord::model($modelClass);

        if (is_array($key)) {
            // The key is an array.
            // Check if there are column names indexing the values in the array.
            reset($key);
            if (key($key) === 0) {
                // There are no attribute names.
                // Check if there are multiple PK values. If there's only one, start again using only the value.
                if (count($key) === 1)
                    return $this->loadModel($key[0], $modelClass);

                // Now we will use the composite PK.
                // Check if the table has composite PK.
                $tablePk = $staticModel->getTableSchema()->primaryKey;
                if (!is_array($tablePk))
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // Check if there are the correct number of keys.
                if (count($key) !== count($tablePk))
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // Get an array of PK values indexed by the column names.
                $pk = $staticModel->fillPkColumnNames($key);

                // Then load the model.
                $model = $staticModel->findByPk($pk);
            } else {
                // There are attribute names.
                // Then we load the model now.
                $model = $staticModel->findByAttributes($key);
            }
        } else {
            // The key is not an array.
            // Check if the table has composite PK.
            $tablePk = $staticModel->getTableSchema()->primaryKey;
            if (is_array($tablePk)) {
                // The table has a composite PK.
                // The key must be a string to have a PK separator.
                if (!is_string($key))
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // There must be a PK separator in the key.
                if (stripos($key, GxActiveRecord::$pkSeparator) === false)
                    throw new CHttpException(400, Yii::t('giix', 'Your request is invalid.'));

                // Generate an array, splitting by the separator.
                $keyValues = explode(GxActiveRecord::$pkSeparator, $key);

                // Start again using the array.
                return $this->loadModel($keyValues, $modelClass);
            } else {
                // The table has a single PK.
                // Then we load the model now.
                $model = $staticModel->findByPk($key);
            }
        }

        // Check if we have a model.
        if ($model === null)
            throw new CHttpException(404, Yii::t('giix', 'The requested page does not exist.'));

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $model->formId)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Outputs (echo) json representation of $data, prints html on debug mode.
     * NOTE: json_encode exists in PHP > 5.2, so it's safe to use it directly without checking
     * @param array $data the data (PHP array) to be encoded into json array
     * @param int $opts Bitmask consisting of JSON_HEX_QUOT, JSON_HEX_TAG, JSON_HEX_AMP, JSON_HEX_APOS, JSON_FORCE_OBJECT.
     */
    public function renderJson($data, $opts=null)
    {
        if(YII_DEBUG && isset($_GET['debug']) && is_array($data))
        {
            foreach($data as $type => $v)
                printf('<h1>%s</h1>%s', $type, is_array($v) ? json_encode($v, $opts) : $v);
        }
        else
        {
            header('Content-Type: application/json; charset=UTF-8');
            echo json_encode($data, $opts);
        }
    }

    /**
     * Utility function to ensure the base url.
     * @param $url
     * @return string
     */
    public function baseUrl( $url = '' )
    {
        static $baseUrl;
        if ($baseUrl === null)
            $baseUrl = Yii::app()->request->baseUrl;
        return $baseUrl . '/' . ltrim($url, '/');
    }

	private $_assets;
	public function getAssets()
	{
		if ($this->_assets === null) {
			$this->_assets = Yii::app()->assetManager->publish(
				Yii::getPathOfAlias('frontend.assets'),
				false,
				-1,
			0  //	YII_DEBUG
			);
		}
		return $this->_assets;
	}

}