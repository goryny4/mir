<?php

Yii::import('common.models._base.BaseTrips');

class Trips extends BaseTrips
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function relations() {
		return array_merge(parent::relations(),array(
			'tripsCountries' => array(self::HAS_MANY, 'TripsCountries', 'trip_id'),
			'countries' => array(self::HAS_MANY, 'Countries', 'country_id', 'through' => 'tripsCountries'),
		));
	}
/* уже не актуально
	public function getMapPath() {
		// video map
		$dir = Yii::app()->basePath.'/www/trips.data/';
		$mapPath = false;
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if (is_dir($dir.$file)) {
						if ((int)$file == $this->id) {
							if ($dh2 = opendir($dir.$file.'/maps/')) {
								while (($file2 = readdir($dh2)) !== false) {
									if (in_array($file2,array('.','..'))) continue;
									$mapPath = '/trips.data/'.$file.'/maps/'.$file2;
									break;
								}
								closedir($dh2);
							}
							break;
						}
					}
				}
				closedir($dh);
			}
		}
		return $mapPath;
	}
*/
}