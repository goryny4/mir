<?php

Yii::import('common.models._base.BasePlaces');

class Places extends BasePlaces
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getImage() {
		$entityDir = strtolower(get_class($this));

		if (is_dir(STATIC_PATH.$entityDir) || mkdir(STATIC_PATH.$entityDir)) {
			if ($dh = opendir(STATIC_PATH.$entityDir)) {
			//	while (($countryDir = readdir($dh)) !== false) {
			//		if (is_dir(STATIC_PATH."$entityDir/$countryDir") && ((int)$countryDir == $this->country_id)) {
			//			if ($dh2 = opendir(STATIC_PATH."$entityDir/$countryDir")) {
							while (($file = readdir($dh)) !== false) {
								if (is_file(STATIC_PATH.($result = "$entityDir/$file"))) {
									if ((int)$file == $this->id)
										return $result;
								}
							}
			}
		}

		return false;
	}

    public function getImageInWidget() {
        $result = ($img = $this->getImage())?
            Yii::app()->controller->renderPartial('image', array(
                'img' => $img
            ),true)
            :'';
        return str_replace(array('<p>','</p>'),'',$result);
    }
}