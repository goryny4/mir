<?php

/**
 * This is the model base class for the table "tbl_plans".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Plans".
 *
 * Columns in table "tbl_plans" available as properties of the model,
 * followed by relations of table "tbl_plans" available as properties of the model.
 *
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $description
 * @property string $content
 * @property string $forum_url
 * @property string $image_url
 * @property integer $active
 *
 * @property Trips[] $trips
 */
abstract class BasePlans extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'tbl_plans';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Plans|Plans', $n);
	}

	public static function representingColumn() {
		return 'title';
	}

	public function rules() {
		return array(
			array('active', 'numerical', 'integerOnly'=>true),
			array('title, date, forum_url, image_url', 'length', 'max'=>255),
			array('description, content', 'safe'),
			array('title, date, description, content, forum_url, image_url, active', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, title, date, description, content, forum_url, image_url, active', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'trips' => array(self::HAS_MANY, 'Trips', 'plan_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'date' => Yii::t('app', 'Date'),
			'description' => Yii::t('app', 'Description'),
			'content' => Yii::t('app', 'Content'),
			'forum_url' => Yii::t('app', 'Forum Url'),
			'image_url' => Yii::t('app', 'Image Url'),
			'active' => Yii::t('app', 'Active'),
			'trips' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('forum_url', $this->forum_url, true);
		$criteria->compare('image_url', $this->image_url, true);
		$criteria->compare('active', $this->active);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}