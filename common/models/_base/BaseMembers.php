<?php

/**
 * This is the model base class for the table "tbl_members".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Members".
 *
 * Columns in table "tbl_members" available as properties of the model,
 * followed by relations of table "tbl_members" available as properties of the model.
 *
 * @property integer $id
 * @property integer $active
 * @property string $name
 * @property string $photo
 * @property string $country
 * @property string $description
 * @property integer $page_id
 *
 * @property Pages $page
 */
abstract class BaseMembers extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'tbl_members';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Members|Members', $n);
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('page_id', 'required'),
			array('active, page_id', 'numerical', 'integerOnly'=>true),
			array('name, photo, country', 'length', 'max'=>45),
			array('description', 'length', 'max'=>255),
			array('active, name, photo, country, description', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, active, name, photo, country, description, page_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'page' => array(self::BELONGS_TO, 'Pages', 'page_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'active' => Yii::t('app', 'Active'),
			'name' => Yii::t('app', 'Name'),
			'photo' => Yii::t('app', 'Photo'),
			'country' => Yii::t('app', 'Country'),
			'description' => Yii::t('app', 'Description'),
			'page_id' => null,
			'page' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('active', $this->active);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('photo', $this->photo, true);
		$criteria->compare('country', $this->country, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('page_id', $this->page_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}