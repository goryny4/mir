<?php
/**********************************************************************************************
*                            CMS Open Business Card
*                              -----------------
*	version				:	1.1.0
*	copyright			:	(c) 2012 Monoray
*	website				:	http://www.monoray.ru/
*	contact us			:	http://www.monoray.ru/contact
*
* This file is part of CMS Open Business Card
*
* Open Business Card is free software. This work is licensed under a GNU GPL.
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
* Open Business Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* Without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
***********************************************************************************************/



class User extends CActiveRecord {

	private static $_saltAddon = 'company';
	public $password_repeat;
	public $old_password;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'tbl_users';
	}

	public function relations(){
		return array(

		);
	}

	public function rules() {
		return array(
			array('email', 'length', 'max' => 64),
			array('old_password, email', 'required', 'on' => 'changeAdminInfo'),
			array('password, password_repeat', 'passValidator', 'on' => 'changeAdminInfo'),
			array('password_repeat', 'safe'),
		);
	}
	
	public function passValidator() {
		if ($this->password) {
			if (empty($this->password_repeat))
				$this->addError('password_repeat', 'Заполните поле');
			if ($this->password != $this->password_repeat)
				$this->addError('password', 'Пароли не совпадают');
			if (utf8_strlen($this->password) < 6)
				$this->addError('password', 'Минимальная длина пароля 6 символов');
		}
	}

	public function attributeLabels() {
		$return = array(
			'password' => 'Пароль',
			'old_password' => 'Прежний пароль',
			'password_repeat' => 'Повторите пароль',
		);
		return $return;
	}

	public function validatePassword($password) {	    
		return self::hashPassword($password, $this->salt) === $this->password;
	}

	public static function hashPassword($password, $salt) {
		return md5($salt . $password . $salt . self::$_saltAddon);
	}

	public static function generateSalt() {
		return uniqid('', true);
	}

	public function setPassword($password = null){
		$this->salt = self::generateSalt();
		if($password == null){
			$password = $this->password;
		}
		$this->password = md5($this->salt . $password . $this->salt . self::$_saltAddon);
	}

	public function randomString($length = 10){
		$chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
		shuffle($chars);
		return implode(array_slice($chars, 0, $length));
	}
}