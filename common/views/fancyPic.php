<p><a class="fancyElm" href="/photos/<?=$path?>">
	<?=Yii::app()->easyImage->thumbOf('frontend/www/'.$path,
	array(
		'resize' => array('width' => ($width?:880),'height'=>($height?:660)),
		//	'rotate' => array('degrees' => 90),
		//	'sharpen' => 50,
		//	'background' => '#ffffff',
		'type' => 'jpg',
		'quality' => ($quality?:80),
	),array('class' => ($class?:'tripImage')))?>
</a></p>
