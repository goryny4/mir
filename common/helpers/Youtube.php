<?php
/**
 * Created by JetBrains PhpStorm.
 * User: GoRyNy4
 * Date: 20.08.13
 * Time: 9:54
 * To change this template use File | Settings | File Templates.
 */

class Youtube {

    /**
     * Get YouTube code from YouTube link
     * @param string link
     * @return string YouTube code
     */
    static public function getCode($link)
    {
        $pos = strpos ($link, '=');
        if($pos !== false)
            return substr($link, $pos+1,11);
        else {
            $pos = strpos ($link, '/');
            if($pos !== false)
                return substr($link, $pos+1,11);
            else
                return 'BbgwEVY4l6I';
        }
    }

	static public function getPlayer($link,$height = '480',$seconds = array())
	{
		if (!isset($seconds['start'])) $seconds['start'] = '26';
		return '<span style="float:left; padding-top:20px; padding-bottom:20px">
		<iframe width="880" height="'.$height.'" src="//www.youtube.com/embed/'.self::getCode($link).'?autoplay=0&rel=0&border=0&showinfo=0&theme=light&color=red&controls=1&autohide=1&disablekb=1&start='.$seconds['start'].(isset($seconds['end'])?'&end='.$seconds['end']:'').'&modestbranding=1" frameborder="0" allowfullscreen></iframe>
		</span>';
	}
}