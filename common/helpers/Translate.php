<?php
/**
 * Created by PhpStorm.
 * User: gor
 * Date: 02.05.2015
 * Time: 10:29
 */

class Translate {

    static function dates($dates) {
        if (Yii::app()->getLanguage() == 'ru') return $dates;

        $dates = mb_strtolower($dates, 'utf-8');

        $mesiatsy = ['январь','февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
        $monthes = ['January','February','March','April','May','June','July','August','September','October','November','December'];

        $dates = str_replace($mesiatsy,$monthes,$dates);
        $dates = str_replace('г.','',$dates);
        return $dates;
    }

    static function length($length) {
        if (Yii::app()->getLanguage() == 'ru') return $length;

        $length = str_replace(['час','мин','сек'],['hour','min','sec'],$length);

        return $length;
    }
}